FROM node:17 AS deps
COPY ./openfoam_cbp/package.json /app/openfoam_cbp/package.json
WORKDIR /app/openfoam_cbp
RUN yarn install

FROM node:17 AS builder
COPY ./openfoam_cbp /app/openfoam_cbp
COPY --from=deps /app/openfoam_cbp/node_modules /app/openfoam_cbp/node_modules
WORKDIR /app/openfoam_cbp
ENV STANDALONE true
RUN yarn run build
ENTRYPOINT [ "npm" ]
CMD ["run", "test:coverage"]

FROM jlaperle/openfoam_cbp:0.2.0 AS runner
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
USER root

RUN curl -sL https://deb.nodesource.com/setup_17.x | bash - &&\
    apt-get install --no-install-recommends -y gcc=4:7.4.0-1ubuntu2.3 g++=4:7.4.0-1ubuntu2.3 make=4.1-9.1ubuntu1 nodejs=17.9.0-1nodesource1 &&\
    npm update --location=global npm

USER nextjs
ENV HOME=/home/nextjs

COPY --from=builder --chown=nextjs:nodejs /app/openfoam_cbp/public /app/public
COPY --from=builder --chown=nextjs:nodejs /app/openfoam_cbp/package.json /app/package.json
COPY --from=builder --chown=nextjs:nodejs /app/openfoam_cbp/.next/standalone /app/
COPY --from=builder --chown=nextjs:nodejs /app/openfoam_cbp/.next/static /app/.next/static

WORKDIR /app
USER nextjs

EXPOSE 3000

ENV PORT 3000

ENTRYPOINT ["/usr/bin/node", "/app/server.js"]