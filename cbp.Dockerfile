FROM openfoam/openfoam6-graphical-apps:6
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
USER root

WORKDIR /app
RUN addgroup --system --gid 1001 nodejs &&\
    adduser --system --uid 1001 nextjs &&\
    chown nextjs:nodejs /app

RUN apt-get update && apt-get install --no-install-recommends -y git=2.17.1-1ubuntu0.11 &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    sed -i 's@^\[ "\$BASH" -o "\$ZSH_NAME" \] \&\& \\@#\[ "\$BASH" -o "\$ZSH_NAME" \] \&\& \\@g' /opt/openfoam6/etc/bashrc &&\
    sed -i "s@\$HOME@/home/nextjs@g" /opt/openfoam6/etc/bashrc

USER nextjs
ENV HOME=/home/nextjs
COPY utils /app/utils
RUN git config --global http.sslverify false &&\
    sh utils/compile_solvers.sh
