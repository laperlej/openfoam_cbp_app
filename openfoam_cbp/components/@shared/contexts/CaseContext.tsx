import { createContext } from 'react'

export const CaseContext = createContext({ state: null, dispatch: null })
