import { Parser } from '../utils/Parser'
import { useFileInfo } from './useFileInfo'

export const useAst = (filePath) => {
  const parser = new Parser()
  const fileInfo = useFileInfo(filePath)
  return parser.parse(fileInfo.text)
}
