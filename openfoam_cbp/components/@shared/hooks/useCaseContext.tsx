import { useContext } from 'react'
import { emptyState } from 'components/@shared/utils/emptyState'
import { CaseContext } from 'components/@shared/contexts/CaseContext'

export const useCaseContext = () => {
  const { state, dispatch } = useContext(CaseContext)
  const solverName = state?.solverName || emptyState.solverName
  const caseFiles = state?.caseFiles || emptyState.caseFiles
  return {
    state: { ...state, solverName: solverName, caseFiles: caseFiles },
    dispatch: dispatch
  }
}
