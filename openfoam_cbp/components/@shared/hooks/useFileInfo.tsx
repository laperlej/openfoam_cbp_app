import { useCaseContext } from './useCaseContext'

export const useFileInfo = (filePath) => {
  const { state } = useCaseContext()
  const fileObj = state.caseFiles[filePath]
  return {
    name: fileObj.data,
    isFolder: fileObj.hasChildren,
    text: fileObj.text,
    path: fileObj.index
  }
}
