import { useContext } from 'react'
import { emptyState } from 'components/@shared/utils/emptyState'
import { CaseContext } from 'components/@shared/contexts/CaseContext'

export const useSolverName = () => {
  const { state } = useContext(CaseContext)
  return state?.solverName || emptyState.solverName
}
