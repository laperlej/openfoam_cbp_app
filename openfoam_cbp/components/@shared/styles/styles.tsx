import styled from '@emotion/styled'
import Button from '@mui/material/Button'
import DialogContent from '@mui/material/DialogContent'
import Box from '@mui/material/Box'

export const CenteredText = styled.div`
  text-align: center;
`

export const BlockSpan = styled.span`
  display: block;
`

export const BlockDiv = styled.div`
  display: block;
`

export const Grid = styled.div<{ gap: string }>`
  display: grid;
  gap: ${(props) => props.gap};
  justify-items: center;
`

export const TwoColumns = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

export const Spacer = styled.div<{ size: string }>`
  height: ${(props) => props.size};
`

export const ImportExportDiv = styled.div`
  height: 100%;
  margin-left: auto;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  gap: 10px;
  margin-right: 40px;
`

export const ImportExportButton = styled(Button)`
  height: 100%;
  margin-top: 5px;
  min-width: 92px;
`

export const DownloadButton = styled(Button)``

export const GridContent = styled(DialogContent)`
  display: grid;
  gap: 1em;
`

export const CenterBox = styled(Box)`
  display: flex;
  justify-content: center;
`

export const VCenterBox = styled(Box)`
  display: flex;
  gap: 0.5em;
  align-items: center;
`

export const DialogBox = styled(DialogContent)`
  height: 200px;
  width: 340px;
  overflow: hidden;
`

export const ImportDialogBox = styled(DialogContent)`
  width: 340px;
  overflow: hidden;
`
