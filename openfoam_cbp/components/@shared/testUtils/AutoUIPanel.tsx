import { UIPanelRouter } from 'components/EditPanel/UIPanelRouter'
import { TestCaseContext } from './TestCaseContext'

export const AutoUIPanel = ({ filePath, solverName = 'hamFoam' }) => {
  return (
    <TestCaseContext solverName={solverName}>
      <UIPanelRouter filePath={filePath} />
    </TestCaseContext>
  )
}
