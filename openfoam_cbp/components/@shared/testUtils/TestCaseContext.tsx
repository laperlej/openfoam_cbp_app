import { getCaseFiles } from 'server/case/caseFiles'
import { CaseContext } from 'components/@shared/contexts/CaseContext'
import { emptyState } from '../utils/emptyState'

export const TestCaseContext = ({ children, solverName = 'hamFoam' }) => {
  const state =
    solverName === null
      ? { emptyState }
      : {
          solverName: solverName,
          caseFiles: getCaseFiles(solverName),
          objFileName: ''
        }

  return (
    <CaseContext.Provider
      value={{
        state: state,
        dispatch: () => {
          /* dummy */
        }
      }}
    >
      {children}
    </CaseContext.Provider>
  )
}
