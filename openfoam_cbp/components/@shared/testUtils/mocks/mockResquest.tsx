import {
  createRequest,
  createResponse,
  createMocks,
  RequestMethod
} from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next'

type ApiRequest = NextApiRequest & ReturnType<typeof createRequest>
type ApiResponse = NextApiResponse & ReturnType<typeof createResponse>

export function mockRequestResponse(method: RequestMethod, query) {
  const { req, res }: { req: ApiRequest; res: ApiResponse } = createMocks<
    ApiRequest,
    ApiResponse
  >({ method })
  req.headers = {
    'Content-Type': 'application/json'
  }
  if (method === 'GET') {
    req.query = query
  } else if (method === 'POST') {
    req.body = query
  }
  return { req, res }
}
