import { LexToken, tokenType } from './Token'

export class Lexer {
  tokens: LexToken[]
  lexDict = {
    '(': tokenType.OpenParenthesis,
    ')': tokenType.CloseParenthesis,
    '{': tokenType.OpenBrace,
    '}': tokenType.CloseBrace,
    '[': tokenType.OpenBracket,
    ']': tokenType.CloseBracket,
    ';': tokenType.Semi
  }
  text: string
  position: [number, number]
  index: number

  constructor() {
    this.init()
  }

  next() {
    if (this.isNewLine()) {
      this.position = [this.position[0] + 1, 1]
    } else {
      this.position[1]++
    }
    this.index++
    return this.text[this.index]
  }

  cur() {
    return this.text[this.index]
  }

  lookAhead() {
    return this.text[this.index + 1]
  }

  isWhiteSpace() {
    return /\s/.test(this.cur())
  }

  isNewLine() {
    return this.cur() == '\n'
  }

  isOperator() {
    return /[(){}[\];]/.test(this.cur())
  }

  isInlineComment() {
    return (
      (this.cur() === '/' && this.lookAhead() === '/') || this.cur() === '#'
    )
  }

  isBlockComment() {
    return this.cur() === '/' && this.lookAhead() === '*'
  }

  isBlockCommentEnder() {
    return this.cur() === '*' && this.lookAhead() === '/'
  }

  isEOF() {
    return this.index >= this.text.length
  }

  isQuote() {
    return this.cur() === '"'
  }

  isString() {
    return (
      !this.isEOF() &&
      !this.isWhiteSpace() &&
      !this.isInlineComment() &&
      !this.isBlockComment() &&
      !this.isOperator()
    )
  }

  whiteSpace() {
    while (this.isWhiteSpace()) {
      this.next()
    }
  }

  string() {
    const startPosition = [...this.position]
    let s = ''
    while (this.isString()) {
      s += this.cur()
      this.next()
    }
    const type = tokenType.String
    const value = s
    const range = [...startPosition, ...this.position]
    this.tokens.push(new LexToken(type, value, range))
  }

  quoteString() {
    const startPosition = [...this.position]
    let s = ''
    s += this.cur()
    this.next()
    while (!this.isQuote()) {
      s += this.cur()
      this.next()
    }
    s += this.cur()
    this.next()
    const type = tokenType.String
    const value = s
    const range = [...startPosition, ...this.position]
    this.tokens.push(new LexToken(type, value, range))
  }

  inlineComment() {
    while (!this.isNewLine() && !this.isEOF()) {
      this.next()
    }
    if (this.isNewLine()) {
      this.next()
    }
  }

  blockComment() {
    while (!this.isBlockCommentEnder() && !this.isEOF()) {
      this.next()
    }
    if (this.isBlockCommentEnder()) {
      this.next()
      this.next()
    }
  }

  operator() {
    const type = this.lexDict[this.cur()]
    const value = this.cur()
    const range = [
      this.position[0],
      this.position[1],
      this.position[0],
      this.position[1] + 1
    ]
    this.tokens.push(new LexToken(type, value, range))
    this.next()
  }

  end() {
    const type = tokenType.EOS
    const value = ''
    const range = [...this.position, ...this.position]
    this.tokens.push(new LexToken(type, value, range))
  }

  start() {
    let i = 0
    //break if too long
    while (i++ < 100000) {
      if (this.isEOF()) {
        this.end()
        return this.tokens
      } else if (this.isWhiteSpace()) {
        this.whiteSpace()
      } else if (this.isOperator()) {
        this.operator()
      } else if (this.isInlineComment()) {
        this.inlineComment()
      } else if (this.isBlockComment()) {
        this.blockComment()
      } else if (this.isQuote()) {
        this.quoteString()
      } else {
        this.string()
      }
    }
  }

  init() {
    this.text = ''
    this.index = 0
    this.position = [1, 1] //line, column
    this.tokens = []
  }

  lex(text) {
    this.text = text
    return this.start()
  }
}
