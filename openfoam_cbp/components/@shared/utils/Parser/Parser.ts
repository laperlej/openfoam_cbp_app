/*
  -----------------RULES-----------------
  File -> Assignment' $
  
  Assignment' -> Assignment Assignment' | Assignment
  Assignment -> Object | Statement
  
  Object -> String { Assignment' }
  Statement -> String value' Semi
  
  Value' -> Value Value' | Value
  Value -> Array | String | AnonymousObject
  Array -> ( Value' ) | [ Value' ]
  AnonymousObject -> { Assignment' }

  String' -> String String' | String
  
  -----------------FIRSTS------------------
  File = String
  Assignment' = String
  Assignment = String
  Object = String
  Statement = String
  Value' = ( String
  Value = ( String
  Array = [ (
  String' = String
*/

import { tokenType, Token, TokenFactory, NullToken } from './Token'
import { Lexer } from './Lexer'

export class Parser {
  tokens: Token[]
  index: number

  next() {
    return this.tokens[this.index++]
  }

  cur() {
    return this.tokens[this.index]
  }

  lookAhead() {
    return this.tokens[this.index + 1]
  }

  isEnd() {
    return this.cur().type === tokenType.EOS
  }

  isString() {
    return this.cur().type === tokenType.String
  }

  isObject() {
    return (
      this.cur().type === tokenType.String &&
      this.lookAhead().type === tokenType.OpenBrace
    )
  }

  isObjectEnd() {
    return this.cur().type === tokenType.CloseBrace
  }

  isStatementEnd() {
    return this.cur().type === tokenType.Semi
  }

  isArray() {
    return (
      this.cur().type === tokenType.OpenBracket ||
      this.cur().type === tokenType.OpenParenthesis
    )
  }

  isArrayEnd() {
    return (
      this.cur().type === tokenType.CloseBracket ||
      this.cur().type === tokenType.CloseParenthesis
    )
  }

  isAnonymousObject() {
    return this.cur().type === tokenType.OpenBrace
  }

  parseObject(parentObj) {
    const startToken = this.cur()
    const value = {}
    this.next() //skip object name
    this.next() //skip '{'

    while (!this.isObjectEnd()) {
      this.parseAssignment(value)
    }
    parentObj[startToken.value] = TokenFactory.build(
      startToken,
      this.cur(),
      tokenType.Object,
      value
    )
    this.next()
  }

  parseArray(parentArr) {
    const value = []
    const startToken = this.cur()
    this.next() //skip '[' or '('

    while (!this.isArrayEnd()) {
      this.parseValue(value)
    }

    parentArr.push(
      TokenFactory.build(startToken, this.cur(), tokenType.Array, value)
    )
    this.next() //skip ']' or ')'
  }

  parseAnonymousObject(parentArr) {
    const value = {}
    const startToken = this.cur()
    this.next() //skip '{'

    while (!this.isObjectEnd()) {
      this.parseAssignment(value)
    }

    parentArr.push(
      TokenFactory.build(
        startToken,
        this.cur(),
        tokenType.AnonymousObject,
        value
      )
    )
    this.next() //skip '}'
  }

  parseValue(parentArr) {
    if (this.isString()) {
      parentArr.push(this.cur())
      this.next() //move to next token
    } else if (this.isArray()) {
      this.parseArray(parentArr)
    } else if (this.isAnonymousObject()) {
      this.parseAnonymousObject(parentArr)
    } else {
      throw new Error('Unexpected token')
    }
  }

  parseStatement(parentObj) {
    const startToken = this.cur()
    const value = []
    this.next() //skip statement name

    while (!this.isStatementEnd()) {
      this.parseValue(value)
    }

    parentObj[startToken.value] = TokenFactory.build(
      startToken,
      this.cur(),
      tokenType.Statement,
      value
    )
    this.next()
  }

  parseAssignment(parentObj) {
    if (this.isObject()) {
      this.parseObject(parentObj)
    } else {
      this.parseStatement(parentObj)
    }
  }

  parseFile() {
    let i = 0
    const value = {}
    const startToken = this.cur()
    //break if too long
    while (i++ < 100000) {
      if (this.isEnd()) {
        return TokenFactory.build(startToken, this.cur(), tokenType.File, value)
      } else if (this.isString()) {
        this.parseAssignment(value)
      } else {
        throw new Error('Unexpected token')
      }
    }
  }

  init() {
    this.tokens = []
    this.index = 0
  }

  parse(text) {
    this.init()
    const lexer = new Lexer()
    this.tokens = lexer.lex(text)
    try {
      return this.parseFile()
    } catch (e) {
      console.error(e)
      return new NullToken()
    }
  }
}
