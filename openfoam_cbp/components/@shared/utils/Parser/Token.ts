/* eslint-disable no-unused-vars */
export enum tokenType {
  File,
  Assignments,
  Assignment,
  EOS,
  Object,
  Statement,
  AnonymousObject,
  String,
  Strings,
  Semi,
  Array,
  Values,
  Value,
  OpenBrace,
  OpenBracket,
  OpenParenthesis,
  CloseBrace,
  CloseBracket,
  CloseParenthesis
}

export class TokenFactory {
  static build(startToken, endToken, type, value) {
    if (
      type === tokenType.File ||
      type === tokenType.Object ||
      type === tokenType.AnonymousObject
    ) {
      return new ObjToken(...this.getParams(startToken, endToken, type, value))
    }
    if (
      type === tokenType.Assignment ||
      type === tokenType.Statement ||
      type === tokenType.Array
    ) {
      return new ArrayToken(
        ...this.getParams(startToken, endToken, type, value)
      )
    }
    if (type === tokenType.String) {
      return new StringToken(
        ...this.getParams(startToken, endToken, type, value)
      )
    }
  }

  static getParams(startToken, endToken, type, value) {
    const range = [
      startToken.range[0],
      startToken.range[1],
      endToken.range[2],
      endToken.range[3]
    ]
    return [type, value, range] as const
  }
}

export type TokenValue = string | number | null

interface TokenInterface {
  type: tokenType
  value: TokenValue
  range: [number, number, number, number]
  get(_: TokenValue): Token
}

export abstract class Token implements TokenInterface {
  type: tokenType
  value: TokenValue
  range: [number, number, number, number]
  constructor(type, value, range) {
    this.type = type
    this.value = value
    this.range = range
  }

  abstract get(_: TokenValue): Token
}

export class LexToken extends Token {
  constructor(type, value, range) {
    super(type, value, range)
  }

  get(_: TokenValue = null): Token {
    return new NullToken()
  }
}

class ArrayToken extends Token {
  constructor(type, value, range) {
    super(type, value, range)
  }
  get(index: TokenValue = null): Token {
    return this.value[index] || new NullToken()
  }
}

class ObjToken extends Token {
  constructor(type, value, range) {
    super(type, value, range)
  }
  get(key: TokenValue = null): Token {
    return this.value[key] || new NullToken()
  }
}

class StringToken extends Token {
  constructor(type, value, range) {
    super(type, value, range)
  }
  get(_: TokenValue = null): Token {
    return new NullToken()
  }
}

export class NullToken extends Token {
  constructor() {
    super(null, null, null)
  }
  get(_: TokenValue = null) {
    return this
  }
}
