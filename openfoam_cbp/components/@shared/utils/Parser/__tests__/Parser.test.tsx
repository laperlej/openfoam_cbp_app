import '@testing-library/jest-dom'
import { Parser } from '../Parser'
import { getCaseFiles } from 'server/case/caseFiles'
import { getBoundaryTokens } from 'components/EditPanel/uipanels/Conditions/BoundaryCondition'

const sampleConditionFile = getCaseFiles('hamFoam')['0/pc']['text']

describe('Parser', () => {
  it('Parse Objtoken range', () => {
    const parser = new Parser()
    const ast = parser.parse(sampleConditionFile)
    const boundaryFieldTokens = getBoundaryTokens(ast)
    const insideBoundaryToken = boundaryFieldTokens.get('inside')
    const insideBoundaryRange = insideBoundaryToken.range
    expect(insideBoundaryRange).toEqual([24, 5, 31, 6])
  })
})
