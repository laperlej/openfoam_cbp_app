import React from 'react'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import InputLabel from '@mui/material/InputLabel'
import Chip from '@mui/material/Chip'
import Box from '@mui/material/Box'
import FormControl from '@mui/material/FormControl'

interface Props {
  value?: string[]
  label?: string
  options: string[]
  onChange?: (e: SelectChangeEvent<string[]>) => void
  style?: Record<string, string>
}

export const CustomMultiSelect: React.FC<Props> = ({
  value = [],
  label = '',
  options,
  style = { width: '200px' },
  onChange = (_) => null
}) => {
  return (
    <FormControl size="small">
      <InputLabel id="select-label">{label}</InputLabel>
      <Select
        value={value}
        labelId="select-label"
        multiple
        style={{ ...style }}
        onChange={onChange}
        label={label}
        renderValue={(selected) => (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
            {selected.map((item) => (
              <Chip key={item} label={item} />
            ))}
          </Box>
        )}
      >
        {options.map((name) => (
          <MenuItem key={name} value={name}>
            {name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
