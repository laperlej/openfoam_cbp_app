import React from 'react'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'

interface Props {
  value?: string
  label?: string
  variant?: 'outlined' | 'standard' | 'filled'
  options: string[]
  onChange?: (e: SelectChangeEvent<string>) => void
  style?: Record<string, string>
  'data-testid'?: string | null
}

export const CustomSelect: React.FC<Props> = ({
  value = '',
  label = '',
  options,
  style = {},
  variant = 'outlined',
  onChange = (e) => null,
  'data-testid': dataTestId = null
}) => {
  return (
    <FormControl size="small">
      <InputLabel id="select-label">{label}</InputLabel>
      <Select
        style={{ ...style }}
        onChange={onChange}
        value={value}
        variant={variant}
        labelId="select-label"
        label={label}
        inputProps={{
          'data-testid': dataTestId
        }}
        SelectDisplayProps={
          {
            'data-testid': ''.concat(dataTestId, '-btn')
          } as React.HTMLAttributes<HTMLDivElement>
        }
      >
        {options.map((name) => (
          <MenuItem
            data-testid={''.concat(dataTestId, '-', name)}
            key={name}
            value={name}
          >
            {name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
