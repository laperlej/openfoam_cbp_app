import React from 'react'
import TextField from '@mui/material/TextField'

interface Props {
  value?: string | number
  validationFn?: (value: string | number) => boolean
  onChange?: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>
  style?: Record<string, unknown>
  label?: string
  'data-testid'?: string | null
}

export const CustomTextField: React.FC<Props> = ({
  value = '',
  validationFn = (_) => true,
  onChange = (e) => null,
  style = { width: '70px' },
  label = '',
  'data-testid': dataTestId = null
}) => {
  return (
    <TextField
      style={{ ...style }}
      label={validationFn(value) ? label : 'error'}
      error={validationFn(value) ? false : true}
      onChange={onChange}
      defaultValue={value}
      inputProps={{
        'data-testid': dataTestId
      }}
      size="small"
    />
  )
}
