import React, { useState } from 'react'
import { ReflexContainer, ReflexSplitter, ReflexElement } from 'react-reflex'
import { ProjectFileTree } from './ProjectFileTree'
import { useWindowHeight } from 'components/@shared/hooks/useWindowHeight'
import * as Styled from './styles'
import { CenteredText } from 'components/@shared/styles'
import { UIPanelRouter } from './UIPanelRouter'
import 'react-reflex/styles.css'
import 'react-complex-tree/lib/style.css'
import { EditorContext } from 'components/@shared/contexts/EditorContext'
import { TextEditor } from './TextEditor'

export const EditPanel = ({ defaultFileSelect = 'root' }) => {
  const [filePath, setFilePath] = useState(defaultFileSelect)
  const [editor, setEditor] = useState(null)
  const editorDidMount = (
    monacoEditor: monaco.editor.IStandaloneCodeEditor
  ) => {
    setEditor(monacoEditor)
  }
  const windowHeight: () => number = useWindowHeight
  return (
    <ReflexContainer
      orientation="vertical"
      style={{ height: windowHeight() - 48 }}
    >
      <ReflexElement size={250} className="left-pane">
        <Styled.GrayBg className={`pane-content`}>
          <ProjectFileTree onChange={setFilePath} />
        </Styled.GrayBg>
      </ReflexElement>
      <ReflexSplitter />
      <ReflexElement className="middle-pane">
        <div className={'pane-content'}>
          <EditorContext.Provider value={editor}>
            <CenteredText>
              <UIPanelRouter filePath={filePath} />
            </CenteredText>
          </EditorContext.Provider>
        </div>
      </ReflexElement>
      <ReflexSplitter />
      <ReflexElement className="right-pane" style={{ overflow: 'hidden' }}>
        <Styled.GrayBg className={`pane-content`}>
          <TextEditor filePath={filePath} onMount={editorDidMount} />
        </Styled.GrayBg>
      </ReflexElement>
    </ReflexContainer>
  )
}
