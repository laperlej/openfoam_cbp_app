import React, { useContext } from 'react'
import {
  UncontrolledTreeEnvironment,
  Tree,
  StaticTreeDataProvider
} from 'react-complex-tree'
import { ExpendedIcon } from 'components/@shared/icons/ExpendedIcon'
import { ClosedIcon } from 'components/@shared/icons/ClosedIcon'
import { FileIcon } from 'components/@shared/icons/FileIcon'
import { CaseContext } from 'components/@shared/contexts/CaseContext'
import { emptyState } from 'components/@shared/utils/emptyState'

export function ProjectFileTree({ onChange }) {
  const { state } = useContext(CaseContext)
  const data = state?.caseFiles || emptyState.caseFiles
  const cx = (classNames) => classNames.filter((cn) => !!cn).join(' ')

  function getIcon(item, context) {
    if (item.hasChildren) {
      //folder icons
      if (context.isExpanded) {
        return <ExpendedIcon />
      }
      return <ClosedIcon />
    }
    //file icons
    return <FileIcon />
  }

  const renderItemArrow = ({ item, context }) => {
    return (
      <div
        className={cx([
          item.hasChildren && 'rct-tree-item-arrow-hasChildren',
          'rct-tree-item-arrow'
        ])}
        {...context.arrowProps}
      >
        {getIcon(item, context)}
      </div>
    )
  }
  const staticTreeDataProvider = new StaticTreeDataProvider(
    data,
    (item, content) => ({ ...item, content })
  )
  return (
    <UncontrolledTreeEnvironment
      dataProvider={staticTreeDataProvider}
      getItemTitle={(item) => item.data}
      viewState={{}}
      onSelectItems={(items) => onChange(items[0])}
      renderItemArrow={renderItemArrow}
    >
      <Tree treeId="tree-1" rootItem="root" treeLabel="File Tree" />
    </UncontrolledTreeEnvironment>
  )
}
