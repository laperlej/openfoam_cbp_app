import React, { useEffect, useRef, useCallback } from 'react'
import Editor, { useMonaco } from '@monaco-editor/react'
import { useWindowHeight } from 'components/@shared/hooks/useWindowHeight'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'
import { useFileInfo } from 'components/@shared/hooks/useFileInfo'
import { isBash } from './utils/isBash'

export const TextEditor = ({ filePath, onMount }) => {
  const fileInfo = useFileInfo(filePath)
  const fileIsBash = isBash(fileInfo.name)
  const { state, dispatch } = useCaseContext()
  const saveFilesTimeout = useRef(null)
  const monaco = useMonaco()
  const windowHeight: () => number = useWindowHeight

  const saveCaseFiles = useCallback(() => {
    try {
      localStorage.setItem('caseFiles', JSON.stringify(state.caseFiles))
    } finally {
      clearTimeout(saveFilesTimeout.current)
      saveFilesTimeout.current = null
    }
  }, [state.caseFiles])

  useEffect(() => {
    return saveCaseFiles
  }, [saveCaseFiles])

  const handleTextEdit = (editorValue) => {
    if (!saveFilesTimeout.current) {
      saveFilesTimeout.current = setTimeout(saveCaseFiles, 300)
    }
    state.caseFiles[filePath]['text'] = editorValue
    dispatch(state, { caseFiles: state.caseFiles })
  }

  useEffect(() => {
    if (monaco) {
      monaco.editor.defineTheme('gray', {
        base: 'vs',
        inherit: true,
        rules: [],
        colors: {
          'editor.background': '#f6f8fa'
        }
      })
    }
  }, [monaco])

  return (
    <Editor
      options={{
        readOnly: fileIsBash || fileInfo.isFolder,
        minimap: { enabled: false }
      }}
      path={fileInfo.path}
      defaultValue={fileInfo.text}
      defaultLanguage={fileIsBash ? 'shell' : 'csharp'}
      height={windowHeight() - 48}
      theme="gray"
      onMount={onMount}
      onChange={handleTextEdit}
    />
  )
}
