import { ControlDict } from '../uipanels/ControlDict'
import { BlockMeshDict } from '../uipanels/BlockMeshDict'
import { DecomposeParDict } from '../uipanels/DecomposeParDict'
import { HamTransportProperties } from '../uipanels/HamTransportProperties'
import { FvSchemes } from '../uipanels/FvSchemes'
import { FvSolution } from '../uipanels/FvSolution'
import { Turbulence } from '../uipanels/Turbulence'
import { useFileInfo } from 'components/@shared/hooks/useFileInfo'
import { useSolverName } from 'components/@shared/hooks/useSolverName'
import { HelpMessage } from '../utils/HelpMessage'
import { Gravity } from '../uipanels/Gravity'
import { BoundaryCondition } from '../uipanels/Conditions'
import { WindTransport } from '../uipanels/WindTransportProperties'

export const UIPanelRouter = ({ filePath }) => {
  const fileInfo = useFileInfo(filePath)
  const solverName = useSolverName()

  switch (fileInfo.name) {
    case 'Allclean':
    case 'Allrun':
    case 'Allprepare':
    case 'reconstructScript':
    case 'setset.batch':
    case 'buildings.obj':
      return (
        <span data-testid="read-only-warning">
          <br />
          This file will be adjusted automatically.
          <br />
          It cannot be edited manually.
        </span>
      )
    case 'controlDict':
      return <ControlDict key={fileInfo.path} filePath={fileInfo.path} />
    case 'decomposeParDict':
      return <DecomposeParDict key={fileInfo.path} filePath={fileInfo.path} />
    case 'blockMeshDict':
      return <BlockMeshDict key={fileInfo.path} filePath={fileInfo.path} />
    case 'transportProperties':
      if (filePath === 'windDrivenRainFoam/constant/transportProperties') {
        return <WindTransport key={fileInfo.path} filePath={fileInfo.path} />
      } else if (solverName === 'hamFoam') {
        return (
          <HamTransportProperties
            key={fileInfo.path}
            filePath={fileInfo.path}
          />
        )
      } else {
        return null
      }
    case 'fvSchemes':
      return <FvSchemes key={fileInfo.path} />
    case 'fvSolution':
      return <FvSolution key={fileInfo.path} filePath={fileInfo.path} />
    case 'turbulenceProperties':
      return <Turbulence key={fileInfo.path} filePath={fileInfo.path} />
    case 'g':
      if (filePath !== 'windDrivenRainFoam/constant/g') {
        return null
      }
      return <Gravity key={fileInfo.path} filePath={fileInfo.path} />
    case 'U':
    case 'Ts':
    case 'T':
    case 'pc':
    case 'k':
    case 'nut':
    case 'p':
    case 'p_rgh':
    case 'ws':
    case 'gcr':
    case 'alphat':
    case 'epsilon':
      return (
        <BoundaryCondition
          conditionName={fileInfo.name}
          filePath={fileInfo.path}
          key={fileInfo.path}
        />
      )
    default:
      return <HelpMessage />
  }
}
