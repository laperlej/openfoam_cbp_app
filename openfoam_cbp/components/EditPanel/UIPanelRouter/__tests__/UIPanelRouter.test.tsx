import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { AutoUIPanel } from 'components/@shared/testUtils/AutoUIPanel'

describe('UIPanelRouter', () => {
  it('renders hamfoam transportproperties', () => {
    render(<AutoUIPanel filePath={'constant/transportProperties'} />)
    expect(screen.getByTestId('ham-transport-properties'))
  })
})

describe('hamFoam UIPanel', () => {
  it('root', () => {
    render(<AutoUIPanel filePath={'root'} />)
    expect(screen.getByTestId('edit-section-message')).toBeInTheDocument()
  })
  it('1DBlockMesh', () => {
    render(<AutoUIPanel filePath={'system/blockMeshDict'} />)
    expect(screen.getByTestId('table-1-segmentLens')).toHaveValue('0.365')
  })
  it('Conditions', () => {
    render(<AutoUIPanel filePath={'0/pc'} />)
    expect(screen.getByTestId('condition-value-field')).toHaveValue(
      '-7.0291e+07'
    )
  })
  it('ControlDict', () => {
    render(<AutoUIPanel filePath={'system/controlDict'} />)
    expect(screen.getByTestId('edit-controldict-endtime')).toHaveValue(
      '5184000'
    )
  })
  it('DecomposeParDict', () => {
    render(<AutoUIPanel filePath={'system/decomposeParDict'} />)
    expect(screen.getByTestId('scotch-subdomains')).toHaveValue('2')
  })
  it('fvSolution', () => {
    render(<AutoUIPanel filePath={'system/fvSolution'} />)
    expect(screen.getByTestId('table-1-tol')).toHaveValue('1e-20')
  })
  it('hamTransport', () => {
    render(<AutoUIPanel filePath={'constant/transportProperties'} />)
    expect(screen.getByTestId('table-1-rho')).toHaveValue('1600')
  })
})
