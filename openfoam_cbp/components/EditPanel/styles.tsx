import styled from '@emotion/styled'

export const GrayBg = styled.div`
  height: 100%;
  background-color: #f6f8fa;
`
