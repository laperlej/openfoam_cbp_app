import { useState } from 'react'
import { useAst } from 'components/@shared/hooks/useAst'
import { isValidFloat } from './utils/validators'
import * as Styled from 'components/@shared/styles'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { Save } from './utils/Save'
import { editABLConditions, getABLPath } from '../utils/editABLConditions'
import { useSolverName } from 'components/@shared/hooks/useSolverName'
import { useFileInfo } from 'components/@shared/hooks/useFileInfo'
import { useMonaco } from '@monaco-editor/react'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'

const getLabel = (varName: string) => {
  return `Set ${varName}`
}

export const AblConditions = ({
  onSave = () => {
    /* */
  }
}) => {
  const { state, dispatch } = useCaseContext()
  const solverName = useSolverName()
  const ablPath = getABLPath(solverName)
  const ast = useAst(ablPath)
  const ablInfo = useFileInfo(ablPath)
  const monaco = useMonaco()

  const varNames = ['Uref', 'Zref', 'flowDir', 'z0', 'zGround']
  const varTokens = varNames.map((varName) => ast.get(varName))
  const [uRef, setURef] = useState<string>(
    (varTokens[0].get(0).value as string) || '0'
  )
  const [zRef, setZRef] = useState<string>(
    (varTokens[1].get(0).value as string) || '0'
  )
  const [flowDirX, setFlowDirX] = useState<string>(
    (varTokens[2].get(0).get(0).value as string) || '0'
  )
  const [flowDirY, setFlowDirY] = useState<string>(
    (varTokens[2].get(0).get(1).value as string) || '0'
  )
  const [flowDirZ, setFlowDirZ] = useState<string>(
    (varTokens[2].get(0).get(2).value as string) || '0'
  )
  const [z0, setZ0] = useState<string>(
    (varTokens[3].get(1).value as string) || '0'
  )
  const [zGround, setZGround] = useState<string>(
    (varTokens[4].get(1).value as string) || '0'
  )
  const allStates = [uRef, zRef, flowDirX, flowDirY, flowDirZ, z0, zGround]

  //const edits = [{ foamObj: gravityToken, newValue: gravity }]

  const validate = () => {
    return allStates.every((value) => isValidFloat(value))
  }

  const inputFieldsData: [
    string,
    string,
    string,
    React.Dispatch<React.SetStateAction<string>>
  ][] = [
    ['uRef', getLabel('Uref'), uRef, setURef],
    ['zRef', getLabel('Zref'), zRef, setZRef],
    ['z0', getLabel('z0'), z0, setZ0],
    ['zGround', getLabel('zGround'), zGround, setZGround],
    ['flowDirX', 'X', flowDirX, setFlowDirX],
    ['flowDirY', 'Y', flowDirY, setFlowDirY],
    ['flowDirZ', 'Z', flowDirZ, setFlowDirZ]
  ]

  const getInputField = ([id, label, val, setVal]) => {
    return (
      <div key={id}>
        <Styled.BlockSpan>{label}</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '120px' }}
          value={val}
          validationFn={isValidFloat}
          onChange={(event) => setVal(event.target.value)}
          data-testid={`${id}-input`}
        />
      </div>
    )
  }

  const getFlowField = ([id, label, val, setVal]) => {
    return (
      <div key={id}>
        <Styled.Spacer size="0.5em" />
        <span style={{ lineHeight: '40px', verticalAlign: 'middle' }}>
          {label}:{' '}
        </span>
        <CustomTextField
          style={{ width: '70px' }}
          value={val}
          validationFn={isValidFloat}
          onChange={(event) => setVal(event.target.value)}
          data-testid={`${id}-input`}
        />
      </div>
    )
  }

  function handleClick() {
    const tmp: [string, string][] = inputFieldsData.map((data) => [
      data[0],
      data[2]
    ])
    const newValues = new Map<string, string>(tmp)
    const newAbl = editABLConditions(newValues, ablInfo, monaco)
    state.caseFiles[ablInfo.path]['text'] = newAbl
    dispatch(state, { caseFiles: state.caseFiles })
    onSave()
  }

  const inputFields = inputFieldsData.slice(0, 4).map(getInputField)
  const flowFields = inputFieldsData.slice(4).map(getFlowField)

  return (
    <div style={{ width: '400px' }}>
      <Styled.TwoColumns>
        <Styled.Grid gap="1em">{inputFields}</Styled.Grid>
        <div>
          <Styled.BlockSpan>Set flow direction</Styled.BlockSpan>
          {flowFields}
        </div>
      </Styled.TwoColumns>
      <Styled.Spacer size="0.5em" />
      <Save edits={[]} isValid={validate()} onClick={handleClick} />
    </div>
  )
}
