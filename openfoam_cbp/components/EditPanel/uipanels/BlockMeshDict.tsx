import React, { useState } from 'react'
import { Save } from './utils/Save'
import { generateMesh } from './utils/generateMesh'
import { isValidFloat, isValidInt } from './utils/validators'
import { CustomSelect } from 'components/@ui/CustomSelect'
import { CustomTable } from 'components/@ui/CustomTable'
import * as Styled from 'components/@shared/styles'
import { useAst } from 'components/@shared/hooks/useAst'
import { editABLConditions, getABLPath } from '../utils/editABLConditions'
import { editMaterials } from '../utils/editMaterials'
import { useSolverName } from 'components/@shared/hooks/useSolverName'
import { useFileInfo } from 'components/@shared/hooks/useFileInfo'
import { useMonaco } from '@monaco-editor/react'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'

const BlockMeshDictBackground = ({ filePath }) => {
  const ast = useAst(filePath)
  const solverName = useSolverName()
  const ablPath = getABLPath(solverName)
  const ablInfo = useFileInfo(ablPath)
  const monaco = useMonaco()
  const { state, dispatch } = useCaseContext()

  const [rows, setRows] = useState([
    { id: 1, axis: 'X', min: '-20', max: '330', cells: '25' },
    { id: 2, axis: 'Y', min: '-50', max: '230', cells: '20' },
    { id: 3, axis: 'Z', min: '0', max: '140', cells: '10' }
  ])

  const backGroundMesh = {
    xMin: rows[0].min,
    xMax: rows[0].max,
    yMin: rows[1].min,
    yMax: rows[1].max,
    zMin: rows[2].min,
    zMax: rows[2].max,
    xCells: rows[0].cells,
    yCells: rows[1].cells,
    zCells: rows[2].cells
  }

  const columns = [
    {
      field: 'axis',
      headerName: 'Axis',
      editable: false,
      width: 50
    },
    {
      field: 'min',
      headerName: 'Min coordinate',
      editable: true,
      width: 150,
      validate: isValidFloat
    },
    {
      field: 'max',
      headerName: 'Max coordinate',
      editable: true,
      width: 150,
      validate: isValidFloat
    },
    {
      field: 'cells',
      headerName: 'Number of Cells',
      editable: true,
      width: 150,
      validate: isValidInt
    }
  ]

  //map keys to syntax tree
  const edits = Object.keys(backGroundMesh).map((varName) => {
    return {
      foamObj: ast.get('backgroundMesh').get(varName).get(0),
      newValue: backGroundMesh[varName]
    }
  })

  const validate = () => {
    return (
      edits.slice(0, 6).every((x) => isValidFloat(x.newValue)) &&
      edits.slice(6).every((x) => isValidInt(x.newValue))
    )
  }

  function handleClick() {
    const newValue = new Map<string, string>([['zGround', rows[1].min]])
    const newAbl = editABLConditions(newValue, ablInfo, monaco)
    state.caseFiles[ablInfo.path]['text'] = newAbl
    dispatch(state, { caseFiles: state.caseFiles })
  }

  return (
    <>
      <h3>Background mesh generation</h3>
      <Styled.Grid gap="1em">
        <Styled.BlockSpan>
          <b>Important</b>:&nbsp;
          <i>
            Make sure the background mesh covers all the coordinates found in
            the obj file. Use negative coordinates if you have to.
          </i>
        </Styled.BlockSpan>
        <CustomTable rows={rows} columns={columns} setRows={setRows} />
        <Save edits={edits} onClick={handleClick} isValid={validate()} />
      </Styled.Grid>
    </>
  )
}

const BlockMeshDict1D = ({ filePath }) => {
  const ast = useAst(filePath)
  const { state, dispatch } = useCaseContext()
  const setSetPath = 'system/setset.batch'
  const transportPropertiesPath = 'constant/transportProperties'
  const setInfo = useFileInfo(setSetPath)
  const transportInfo = useFileInfo(transportPropertiesPath)
  const monaco = useMonaco()

  const [rows, setRows] = useState([
    { id: 1, segmentLens: '0.365', segmentCells: '100', segmentRatios: '50' },
    { id: 2, segmentLens: '0.015', segmentCells: '15', segmentRatios: '20' },
    { id: 3, segmentLens: '0.04', segmentCells: '30', segmentRatios: '20' },
    { id: 4, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 5, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 6, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 7, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 8, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 9, segmentLens: '1', segmentCells: '1', segmentRatios: '1' },
    { id: 10, segmentLens: '1', segmentCells: '1', segmentRatios: '1' }
  ])

  const [nbLayers, setNbLayers] = useState<number>(3) //has to be based on transportproperties, data has to be saved somewhere
  const segmentLens = rows.map((row) => row.segmentLens)
  const segmentCells = rows.map((row) => row.segmentCells)
  const segmentRatios = rows.map((row) => row.segmentRatios)

  const columns = [
    {
      field: 'id',
      headerName: 'Layer',
      editable: false,
      width: 50
    },
    {
      field: 'segmentLens',
      headerName: 'Thickness',
      editable: true,
      width: 150,
      validate: isValidFloat
    },
    {
      field: 'segmentCells',
      headerName: 'Cells',
      editable: true,
      width: 150,
      validate: isValidInt
    },
    {
      field: 'segmentRatios',
      headerName: 'Cell Ratio Center/Boundary',
      editable: true,
      width: 150,
      validate: isValidFloat
    }
  ]

  const meshData = generateMesh(
    segmentLens.slice(0, nbLayers),
    segmentCells.slice(0, nbLayers),
    segmentRatios
      .slice(0, nbLayers)
      .map((ratio) => `((0.5 0.5 ${ratio})(0.5 0.5 ${1 / parseFloat(ratio)}))`)
  )
  const edits = [
    { foamObj: ast.get('vertices').get(0), newValue: meshData.vertices },
    { foamObj: ast.get('blocks').get(0), newValue: meshData.blocks },
    {
      foamObj: ast.get('patches').get(0).get(2),
      newValue: meshData.patches.inside
    },
    {
      foamObj: ast.get('patches').get(0).get(5),
      newValue: meshData.patches.outside
    },
    {
      foamObj: ast.get('patches').get(0).get(8),
      newValue: meshData.patches.sides
    }
  ]

  const validate = () => {
    return (
      segmentLens.every(isValidFloat) &&
      segmentCells.every(isValidInt) &&
      segmentRatios.every(isValidFloat)
    )
  }
  function handleClick() {
    const { newSetText, newTransportText } = editMaterials(
      meshData.segmentXCoordinates,
      setInfo,
      transportInfo,
      monaco
    )
    state.caseFiles[setInfo.path]['text'] = newSetText
    state.caseFiles[transportInfo.path]['text'] = newTransportText
    dispatch(state, { caseFiles: state.caseFiles })
  }

  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="1em" />
      <Styled.BlockSpan>Choose how many layers of materal</Styled.BlockSpan>
      <CustomSelect
        options={['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']}
        onChange={(event) => setNbLayers(parseInt(event.target.value))}
        value={nbLayers.toString()}
      />
      <CustomTable
        rows={rows.slice(0, nbLayers)}
        columns={columns}
        setRows={setRows}
      />
      <Save edits={edits} onClick={handleClick} isValid={validate()} />
    </Styled.Grid>
  )
}

export const BlockMeshDict = ({ filePath }) => {
  const { state } = useCaseContext()
  if (state.solverName === 'hamFoam') {
    return <BlockMeshDict1D filePath={filePath} />
  } else {
    if (state.objFileName) {
      return <BlockMeshDictBackground filePath={filePath} />
    }
    return (
      <div>
        <Styled.Spacer size="2em" />
        <Styled.BlockSpan>
          Background mesh generation is only supported for custom .obj files.
        </Styled.BlockSpan>
      </div>
    )
  }
}
