import React, { useState } from 'react'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { Save } from 'components/EditPanel/uipanels/utils/Save'
import { isValidFloat } from '../utils/validators'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../../@shared/hooks/useAst'
import { Token, tokenType } from '../../../@shared/utils/Parser/Token'
import {
  boundaryOptions,
  getDefaultBoundaryValue
} from './utils/boundaryOptions'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import Link from '@mui/material/Link'
import styled from '@emotion/styled'
import TableContainer from '@mui/material/TableContainer'
import TableRow from '@mui/material/TableRow'
import Table from '@mui/material/Table'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import TextField from '@mui/material/TextField'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import { AblConditions } from 'components/EditPanel/uipanels/AblConditions'

export const getBoundaryTokens = (ast) => {
  return ast.get('boundaryField')
}

const getInternalFieldToken = (ast) => {
  const internalValueToken = ast.get('internalField').get(1)
  return getInternalValue(internalValueToken)
}

const getInternalValue = (token) => {
  if (token.type === tokenType.Array) {
    return token.get(0)
  }
  return token
}

const getBoundaryFields = (ast): Record<string, Token> => {
  return ast.get('boundaryField').value || {}
}

const getBoundaryTypes = (boundaryFields) => {
  const boundaryTypes = {}
  for (const boundaryName in boundaryFields) {
    boundaryTypes[boundaryName] = boundaryFields[boundaryName]
      .get('type')
      .get(0)
      .value.replace('compressible::', '')
  }
  return boundaryTypes
}

const getBoundaryNames = (ast) => {
  const boundaryObj = getBoundaryTokens(ast).value
  if (boundaryObj) {
    return Object.keys(boundaryObj)
  }
  return []
}

const getBoundaryValueTokens = (ast) => {
  const boundaryValueTokens = new Map<string, Token>()
  const boundaryFields = getBoundaryFields(ast)
  for (const boundaryName of Object.keys(boundaryFields)) {
    const variables = boundaryFields[boundaryName]
    boundaryValueTokens.set(
      boundaryName,
      getInternalValue(variables.get('value').get(1))
    )
  }
  return boundaryValueTokens
}

const boundaryTypeSelectSection = (
  boundaryTypes,
  setBoundaryTypes,
  conditionName
) => {
  const selects = []
  const boundaryNames = Object.keys(boundaryTypes)
  for (const boundaryName of boundaryNames) {
    if (boundaryOptions.get(conditionName)?.[boundaryName]) {
      selects.push(
        <TableRow key={boundaryName}>
          <TableCell padding="none">
            <TextField
              style={{ width: '100px' }}
              value={boundaryName}
              variant="standard"
              InputProps={{
                readOnly: true
              }}
            />
          </TableCell>
          <TableCell padding="none">
            <BoundaryTypeSelect
              key={boundaryName}
              boundaryType={boundaryTypes[boundaryName]}
              setBoundaryType={(boundaryType) =>
                setBoundaryTypes({
                  ...boundaryTypes,
                  [boundaryName]: boundaryType
                })
              }
              conditionName={conditionName}
              boundaryName={boundaryName}
            />
          </TableCell>
        </TableRow>
      )
    }
  }
  if (selects.length > 0) {
    return (
      <>
        <Styled.BlockSpan>Change boundary type</Styled.BlockSpan>
        <TableContainer>
          <Table
            style={{ margin: 'auto', textAlign: 'center' }}
            sx={{ align: 'center', width: '350px' }}
          >
            <TableBody>{selects}</TableBody>
          </Table>
        </TableContainer>
      </>
    )
  } else {
    return null
  }
}

const join = (between: string, strings: string[]): string => {
  if (strings.length === 0) {
    return ''
  }
  let finalString = strings[0]
  for (const s of strings.slice(1)) {
    finalString += between + s
  }
  return finalString
}

const BoundaryTypeSelect = ({
  boundaryType,
  setBoundaryType,
  conditionName,
  boundaryName
}) => {
  const options = boundaryOptions.get(conditionName)[boundaryName]
  const dataTestId = join('-', [
    conditionName,
    boundaryName,
    'boundary',
    'dropdown'
  ])
  return (
    <Select
      style={{ width: '250px' }}
      value={boundaryType}
      variant="standard"
      onChange={(event) => setBoundaryType(event.target.value)}
      inputProps={{
        'data-testid': dataTestId
      }}
      SelectDisplayProps={
        {
          'data-testid': ''.concat(dataTestId, '-btn')
        } as React.HTMLAttributes<HTMLDivElement>
      }
    >
      {options.map((item) => (
        <MenuItem
          key={item}
          value={item}
          data-testid={''.concat(dataTestId, '-', item)}
        >
          {item}
        </MenuItem>
      ))}
    </Select>
  )
}

export const BoundaryCondition = ({ filePath, conditionName }) => {
  const ast: Token = useAst(filePath)
  //get the token for internal value
  const internalValueToken = getInternalFieldToken(ast)
  //get current internal value
  const currentInternalValue = internalValueToken.value
  const [internalValue, setInternalValue] = useState(
    currentInternalValue || '0'
  )

  //get the token for each boundary from the ast
  const boundaryFields: Record<string, Token> = getBoundaryFields(ast)
  //get the current type of each boundary
  const currentBoundaryTypes = getBoundaryTypes(boundaryFields)
  const [boundaryTypes, setBoundaryTypes] = useState<Record<string, string>>(
    currentBoundaryTypes || {}
  )

  const getBoundaryEdits = () => {
    const boundaryTokens = getBoundaryTokens(ast)
    const boundaryNames = getBoundaryNames(ast)
    const boundaryValueTokens = getBoundaryValueTokens(ast)
    const boundaryEdits = [
      {
        foamObj: internalValueToken,
        newValue: internalValue
      }
    ]
    for (const boundaryName of boundaryNames) {
      if (currentBoundaryTypes[boundaryName] !== boundaryTypes[boundaryName]) {
        boundaryEdits.push({
          foamObj: boundaryTokens.get(boundaryName),
          newValue: getDefaultBoundaryValue(
            conditionName,
            boundaryName,
            boundaryTypes[boundaryName],
            internalValue
          )
        })
      } else if (
        boundaryValueTokens.get(boundaryName)?.value === currentInternalValue
      ) {
        boundaryEdits.push({
          foamObj: boundaryValueTokens.get(boundaryName),
          newValue: internalValue
        })
      }
    }
    return boundaryEdits
  }

  const edits = getBoundaryEdits()

  const validate = () => {
    return isValidFloat(internalValue)
  }

  const boundaryTypeTable = boundaryTypeSelectSection(
    boundaryTypes,
    setBoundaryTypes,
    conditionName
  )

  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="1em" />
      <Styled.BlockSpan>Change boundary field value</Styled.BlockSpan>
      <CustomTextField
        style={{ width: '150px' }}
        value={internalValue}
        validationFn={isValidFloat}
        onChange={(event) => setInternalValue(event.target.value)}
        data-testid="condition-value-field"
      />
      <Styled.Spacer size="1em" />
      {boundaryTypeTable}
      {['U', 'k', 'epsilon'].includes(conditionName) && <EditAbl />}
      <Save edits={edits} isValid={validate()} />
    </Styled.Grid>
  )
}

const StyledLink = styled(Link)`
  &:hover {
    cursor: pointer;
  }
`

const EditAbl = () => {
  const [open, setOpen] = useState(false)
  return (
    <>
      <StyledLink onClick={() => setOpen(true)}>Edit ABLconditions</StyledLink>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle sx={{ textAlign: 'center' }}>
          Edit ABLconditions
        </DialogTitle>
        <DialogContent sx={{ textAlign: 'center' }}>
          <AblConditions onSave={() => setOpen(false)} />
        </DialogContent>
      </Dialog>
    </>
  )
}
