import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import { AutoUIPanel } from 'components/@shared/testUtils/AutoUIPanel'

describe('pc', () => {
  it('inside dropdown', async () => {
    const user = userEvent.setup()
    render(<AutoUIPanel filePath={'0/pc'} />)
    //inside
    const insideDropdown = screen.getByTestId('pc-inside-boundary-dropdown')
    expect(insideDropdown).toHaveValue('HAMexternalMoistureFlux')
    //outside
    const outsideDropdown = screen.getByTestId('pc-outside-boundary-dropdown')
    expect(outsideDropdown).toHaveValue('HAMexternalMoistureFlux')
    //click
    const selectBtn = screen.getByTestId('pc-inside-boundary-dropdown-btn')
    await user.click(selectBtn)
    //options
    expect(
      screen.getByTestId('pc-inside-boundary-dropdown-HAMexternalMoistureFlux')
    ).toBeInTheDocument()
    expect(
      screen.getByTestId('pc-inside-boundary-dropdown-HAMexternalImpermeable')
    ).toBeInTheDocument()
  })
})

describe('Ts', () => {
  it('inside dropdown', () => {
    render(<AutoUIPanel filePath={'0/Ts'} />)
    //inside
    const insideDropdown = screen.getByTestId('Ts-inside-boundary-dropdown')
    expect(insideDropdown).toHaveValue('HAMexternalHeatFlux')
    //outside
    const outsideDropdown = screen.getByTestId('Ts-outside-boundary-dropdown')
    expect(outsideDropdown).toHaveValue('HAMexternalHeatFlux')
  })
})

describe('ws', () => {
  it('inside dropdown', () => {
    render(<AutoUIPanel filePath={'0/ws'} />)
    //inside
    const insideDropdown = screen.getByTestId('ws-inside-boundary-dropdown')
    expect(insideDropdown).toHaveValue('zeroGradient')
    //outside
    const outsideDropdown = screen.getByTestId('ws-outside-boundary-dropdown')
    expect(outsideDropdown).toHaveValue('zeroGradient')
  })
})
