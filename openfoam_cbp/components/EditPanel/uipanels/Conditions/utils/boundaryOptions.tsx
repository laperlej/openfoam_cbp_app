const hamOptions = {
  pc: ['HAMexternalMoistureFlux', 'HAMexternalImpermeable'],
  Ts: ['HAMexternalHeatFlux'],
  ws: ['zeroGradient']
}

const directionOptions = {
  U: ['atmBoundaryLayerInletVelocity', 'zeroGradient', 'slip', 'inletOutlet'],
  T: ['uniformFixedValue', 'inletOutlet', 'slip'],
  w: ['uniformFixedValue', 'inletOutlet', 'slip'],
  epsilon: ['atmBoundaryLayerInletEpsilon', 'slip', 'zeroGradient'],
  p_rgh: ['fixedFluxPressure', 'fixedValue', 'slip'],
  alphat: ['calculated', 'slip'],
  gcr: ['fixedValue', 'slip'],
  k: ['atmBoundaryLayerInletK', 'zeroGradient', 'slip'],
  nut: ['calculated', 'slip'],
  p: ['calculated', 'slip']
}

const wallOptions = {
  U: ['fixedValue'],
  T: ['fixedValue', 'zeroGradient'],
  w: ['fixedValue'],
  epsilon: ['epsilonWallFunction'],
  p_rgh: ['fixedFluxPressure'],
  alphat: ['alphatWallFunction', 'alphatJayatillekeWallFunction'],
  gcr: ['fixedValue'],
  k: ['kqRWallFunction'],
  nut: ['nutkWallFunction'],
  p: ['calculated']
}

const groundOptions = {
  nut: ['nutkAtmRoughWallFunction'],
  w: ['zeroGradient']
}

export const boundaryOptions = new Map<string, unknown>([
  ['pc', { inside: hamOptions.pc, outside: hamOptions.pc }],
  ['Ts', { inside: hamOptions.Ts, outside: hamOptions.Ts }],
  ['ws', { inside: hamOptions.ws, outside: hamOptions.ws }],
  [
    'U',
    {
      north: directionOptions.U,
      south: directionOptions.U,
      west: directionOptions.U,
      east: directionOptions.U,
      ground: wallOptions.U,
      top: ['slip'],
      building: wallOptions.U
    }
  ],
  [
    'T',
    {
      north: directionOptions.T,
      south: directionOptions.T,
      west: directionOptions.T,
      east: directionOptions.T,
      ground: wallOptions.T,
      top: ['slip'],
      building: wallOptions.T
    }
  ],
  [
    'w',
    {
      north: directionOptions.w,
      south: directionOptions.w,
      west: directionOptions.w,
      east: directionOptions.w,
      ground: groundOptions.w,
      top: ['slip'],
      building: wallOptions.w
    }
  ],
  [
    'epsilon',
    {
      north: directionOptions.epsilon,
      south: directionOptions.epsilon,
      west: directionOptions.epsilon,
      east: directionOptions.epsilon,
      ground: wallOptions.epsilon,
      top: ['slip'],
      building: wallOptions.epsilon
    }
  ],
  [
    'p_rgh',
    {
      north: directionOptions.p_rgh,
      south: directionOptions.p_rgh,
      west: directionOptions.p_rgh,
      east: directionOptions.p_rgh,
      ground: wallOptions.p_rgh,
      top: ['slip'],
      building: wallOptions.p_rgh
    }
  ],
  [
    'alphat',
    {
      north: directionOptions.alphat,
      south: directionOptions.alphat,
      west: directionOptions.alphat,
      east: directionOptions.alphat,
      ground: wallOptions.alphat,
      top: ['slip'],
      building: wallOptions.alphat
    }
  ],
  [
    'gcr',
    {
      north: directionOptions.gcr,
      south: directionOptions.gcr,
      west: directionOptions.gcr,
      east: directionOptions.gcr,
      ground: wallOptions.gcr,
      top: ['slip'],
      building: wallOptions.gcr
    }
  ],
  [
    'k',
    {
      north: directionOptions.k,
      south: directionOptions.k,
      west: directionOptions.k,
      east: directionOptions.k,
      ground: wallOptions.k,
      top: ['slip'],
      building: wallOptions.k
    }
  ],
  [
    'nut',
    {
      north: directionOptions.nut,
      south: directionOptions.nut,
      west: directionOptions.nut,
      east: directionOptions.nut,
      ground: groundOptions.nut,
      top: ['slip'],
      building: wallOptions.nut
    }
  ],
  [
    'p',
    {
      north: directionOptions.p,
      south: directionOptions.p,
      west: directionOptions.p,
      east: directionOptions.p,
      ground: wallOptions.p,
      top: ['slip'],
      building: wallOptions.p
    }
  ]
])

function getKwargs(conditionName, boundaryName, boundaryType, internalValue) {
  const typeSpecificKwargsDict = {
    U: {
      inletOutlet: { value: `(${internalValue} 0 0)`, inletValue: '(0 0 0)' },
      fixedValue: { value: '(0 0 0)' }
    },
    T: {
      inletOutlet: { inletValue: internalValue },
      fixedValue: { value: '300.0' },
      uniformFixedValue: { file: '0/air/Tambient' }
    },
    nut: {
      nutkWallFunction: {
        Cmu: '0.09',
        kappa: '0.41',
        E: '9.8'
      }
    },
    p: { fixedValue: { value: '0' } },
    w: {
      uniformFixedValue: { file: '0/air/wambient' },
      inletOutlet: { inletValue: internalValue },
      fixedValue: { value: '0.008' }
    },
    epsilon: {
      epsilonWallFunction: {
        Cmu: '0.09',
        kappa: '0.41',
        E: '9.8'
      }
    }
  }
  const genericKwargs = { boundaryName: boundaryName, value: internalValue }
  const typeSpecificKwargs =
    typeSpecificKwargsDict?.[conditionName]?.[boundaryType] || {}
  return { ...genericKwargs, ...typeSpecificKwargs }
}

const boundaryTypeDict = {
  HAMexternalMoistureFlux: `{{boundaryName}}
    {
        type                    HAMexternalMoistureFlux;
        value                   uniform {{value}}; 
        beta                    {file "0/inside/beta";}
        pv_o                    {file "0/inside/pv_o";}
        gl                      {file "0/inside/gl";}         
    }`,
  HAMexternalImpermeable: `{{boundaryName}}
    {
        type                    HAMexternalImpermeable;
        value                   uniform {{value}}; 
        beta                    {file "0/inside/beta";}
        pv_o                    {file "0/inside/pv_o";}
        gl                      {file "0/inside/gl";}         
    }`,
  HAMexternalHeatFlux: `{{boundaryName}}
    {     
        type                    HAMexternalHeatFlux;  
        value                   uniform {{value}};  
        Tambient                {file "0/inside/Tambient";}
        alpha                   {file "0/inside/alpha";}        
        rad                     {file "0/inside/rad";} 
        beta                    {file "0/inside/beta";}
        pv_o                    {file "0/inside/pv_o";}
        gl                      {file "0/inside/gl";}
        rainTemp                {file "0/inside/rainTemp";}         
    }`,
  zeroGradient: `{{boundaryName}}
    {
        type            zeroGradient;
    }`,
  slip: `{{boundaryName}}
    {
        type            slip;
    }`,
  atmBoundaryLayerInletEpsilon: `{{boundaryName}}
    {
        type            atmBoundaryLayerInletEpsilon;
        #include        "include/ABLConditions"
    }`,
  epsilonWallFunction: `{{boundaryName}}
    {
        type            epsilonWallFunction;
        Cmu             {{Cmu}};
        kappa           {{kappa}};
        E               {{E}};
        value           uniform {{value}}; 
    }`,
  atmBoundaryLayerInletVelocity: `{{boundaryName}}
    {
        type            atmBoundaryLayerInletVelocity;
        #include        "include/ABLConditions"
    }`,
  inletOutlet: `{{boundaryName}}
    {
        type            inletOutlet;
        inletValue      uniform {{inletValue}};
        value           uniform {{value}}; 
    }`,
  fixedValue: `{{boundaryName}}
    {
        type            fixedValue;
        value           uniform {{value}};
    }`,
  uniformFixedValue: `{{boundaryName}}
    {
        type            uniformFixedValue;
        uniformValue    tableFile;
        uniformValueCoeffs
        {
            file        "$FOAM_CASE/{{file}}";
        }
        value           uniform {{value}}; 
    }`,
  fixedFluxPressure: `{{boundaryName}}
    {
        type            fixedFluxPressure;
        gradient        uniform 0; 
        value           uniform {{value}}; 
    }`,
  calculated: `{{boundaryName}}
    {
        type            calculated;
        value           uniform {{value}}; 
    }`,
  alphatWallFunction: `{{boundaryName}}
    {
        type            compressible::alphatWallFunction;
        Prt             0.85;
        value           uniform {{value}}; 
    }`,
  alphatJayatillekeWallFunction: `{{boundaryName}}
    {
        type            compressible::alphatJayatillekeWallFunction;
        Prt             0.85;
        value           uniform {{value}}; 
    }`,
  atmBoundaryLayerInletK: `{{boundaryName}}
    {
        type            atmBoundaryLayerInletK;
        #include        "include/ABLConditions"
    }`,
  kqRWallFunction: `{{boundaryName}}
    {
        type            kqRWallFunction;
        value           uniform {{value}}; 
    }`,
  nutkAtmRoughWallFunction: `{{boundaryName}}
    {
        type            nutkAtmRoughWallFunction;
        z0              $z0;
        value           uniform 0.0;
    }`,
  nutkWallFunction: `{{boundaryName}}
    {
        type            nutkWallFunction;
        Cmu             0.09;
        kappa           0.41;
        E               9.8;
        value           uniform {{value}}; 
    }`
}

export function getDefaultBoundaryValue(
  conditionName: string,
  boundaryName: string,
  boundaryType: string,
  internalFieldValue: string
) {
  const kwargs = getKwargs(
    conditionName,
    boundaryName,
    boundaryType,
    internalFieldValue
  )
  let s: string = boundaryTypeDict[boundaryType]
  for (const key in kwargs) {
    s = s.replace(`{{${key}}}`, kwargs[key])
  }
  return s
}
