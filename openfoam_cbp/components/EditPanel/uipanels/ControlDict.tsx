import React, { useState } from 'react'
import { Save } from './utils/Save'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { isValidInt } from './utils/validators'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../@shared/hooks/useAst'

export const ControlDict = ({ filePath }) => {
  const ast = useAst(filePath)

  const getToken = (varName) => {
    return ast.get(varName).get(0)
  }

  const [startTime, setStartTime] = useState(getToken('startTime').value || '0')
  const [endTime, setEndTime] = useState(getToken('endTime').value || '0')
  const [deltaT, setDeltaT] = useState(getToken('deltaT').value || '0')
  const [writeInterval, setWriteInterval] = useState(
    getToken('writeInterval').value || '0'
  )

  const edits = [
    { foamObj: getToken('startTime'), newValue: startTime },
    { foamObj: getToken('endTime'), newValue: endTime },
    { foamObj: getToken('deltaT'), newValue: deltaT },
    { foamObj: getToken('writeInterval'), newValue: writeInterval }
  ]

  const validate = () => {
    return (
      isValidInt(startTime) &&
      isValidInt(endTime) &&
      isValidInt(deltaT) &&
      isValidInt(writeInterval)
    )
  }

  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="0.5em" />
      <div>
        <Styled.BlockSpan>Choose start time.</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '120px' }}
          value={startTime}
          validationFn={isValidInt}
          onChange={(event) => setStartTime(event.target.value)}
        />
      </div>
      <div>
        <Styled.BlockSpan>Choose end time.</Styled.BlockSpan>

        <CustomTextField
          style={{ width: '120px' }}
          value={endTime}
          validationFn={isValidInt}
          onChange={(event) => setEndTime(event.target.value)}
          data-testid="edit-controldict-endtime"
        />
      </div>
      <div>
        <Styled.BlockSpan>Time between each step (deltaT).</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '120px' }}
          value={deltaT}
          validationFn={isValidInt}
          onChange={(event) => setDeltaT(event.target.value)}
        />
      </div>
      <div>
        <Styled.BlockSpan>Choose write interval.</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '120px' }}
          value={writeInterval}
          validationFn={isValidInt}
          onChange={(event) => setWriteInterval(event.target.value)}
          data-testid="edit-controldict-writeinterval"
        />
      </div>
      <Save edits={edits} isValid={validate()} />
    </Styled.Grid>
  )
}
