import React, { useState } from 'react'
import { Save } from './utils/Save'
import { isValidNonZero2Digit } from './utils/validators'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { CustomSelect } from 'components/@ui/CustomSelect'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../@shared/hooks/useAst'

export const DecomposeParDict = ({ filePath }) => {
  const ast = useAst(filePath)
  const [method, setMethod] = useState<string>(
    (ast.get('method').get(0).value as string) || 'scotch'
  )
  const [nbSubdomains, setNbSubdomains] = useState(
    ast.get('numberOfSubdomains').get(0).value || '2'
  )
  const [simpleX, setSimpleX] = useState(
    ast.get('simpleCoeffs').get('n').get(0).get(0).value || '1'
  )
  const [simpleY, setSimpleY] = useState(
    ast.get('simpleCoeffs').get('n').get(0).get(1).value || '1'
  )
  const [simpleZ, setSimpleZ] = useState(
    ast.get('simpleCoeffs').get('n').get(0).get(2).value || '1'
  )
  const methodChoices = ['scotch', 'simple']
  const edits = [
    { foamObj: ast.get('method').get(0), newValue: method },
    {
      foamObj: ast.get('numberOfSubdomains').get(0),
      newValue:
        method === 'scotch'
          ? nbSubdomains
          : String(Number(simpleX) * Number(simpleY) * Number(simpleZ))
    },
    {
      foamObj: ast.get('simpleCoeffs').get('n').get(0).get(0),
      newValue: simpleX
    },
    {
      foamObj: ast.get('simpleCoeffs').get('n').get(0).get(1),
      newValue: simpleY
    },
    {
      foamObj: ast.get('simpleCoeffs').get('n').get(0).get(2),
      newValue: simpleZ
    }
  ]

  const validate = () => {
    return (
      isValidNonZero2Digit(nbSubdomains) &&
      isValidNonZero2Digit(simpleX) &&
      isValidNonZero2Digit(simpleY) &&
      isValidNonZero2Digit(simpleZ)
    )
  }
  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="1em" />
      <Styled.BlockSpan>Choose a decomposition method</Styled.BlockSpan>
      <CustomSelect
        options={methodChoices}
        onChange={(event) => setMethod(event.target.value)}
        value={method}
      />
      {method === 'scotch' && (
        <div>
          <Styled.BlockSpan>Choose the number of subdomains.</Styled.BlockSpan>
          <CustomTextField
            style={{ width: '50px' }}
            value={nbSubdomains}
            validationFn={isValidNonZero2Digit}
            onChange={(event) => setNbSubdomains(event.target.value)}
            data-testid="scotch-subdomains"
          />
        </div>
      )}
      {method === 'simple' && (
        <>
          <div>
            <span style={{ lineHeight: '40px', verticalAlign: 'middle' }}>
              X:{' '}
            </span>
            <CustomTextField
              style={{ width: '50px' }}
              value={simpleX}
              validationFn={isValidNonZero2Digit}
              onChange={(event) => setSimpleX(event.target.value)}
            />
          </div>
          <div>
            <span style={{ lineHeight: '40px', verticalAlign: 'middle' }}>
              Y:{' '}
            </span>
            <CustomTextField
              style={{ width: '50px' }}
              value={simpleY}
              validationFn={isValidNonZero2Digit}
              onChange={(event) => setSimpleY(event.target.value)}
            />
          </div>
          <div>
            <span style={{ lineHeight: '40px', verticalAlign: 'middle' }}>
              Z:{' '}
            </span>
            <CustomTextField
              style={{ width: '50px' }}
              value={simpleZ}
              validationFn={isValidNonZero2Digit}
              onChange={(event) => setSimpleZ(event.target.value)}
            />
          </div>
        </>
      )}
      <Save edits={edits} isValid={validate()} />
    </Styled.Grid>
  )
}
