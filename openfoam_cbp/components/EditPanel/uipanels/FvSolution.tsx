import React, { useState } from 'react'
import { Save } from './utils/Save'
import { isValidFloat } from './utils/validators'
import { CustomTable } from 'components/@ui/CustomTable'
import { range } from './utils/range'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../@shared/hooks/useAst'
import { Token } from '../../@shared/utils/Parser/Token'

function init_rows(ast) {
  const variable = Object.keys(ast.get('solvers').value || {})
  const nbLayers = variable.length || 0
  const solversToken = ast.get('solvers')

  const getValues = (varName) =>
    Object.values(solversToken.value || {}).map(
      (varInfo: Token) => varInfo.get(varName).get(0).value
    )

  const tolerance = getValues('tolerance')
  const relTol = getValues('relTol')

  return range(1, nbLayers + 1).map((i) => {
    return {
      id: i,
      var: variable[i - 1],
      tol: tolerance[i - 1],
      relTol: relTol[i - 1]
    }
  })
}

export const FvSolution = ({ filePath }) => {
  const ast = useAst(filePath)
  const solversToken = ast.get('solvers')
  const [rows, setRows] = useState(init_rows(ast))
  const columns = [
    {
      field: 'var',
      headerName: 'Variable',
      editable: false,
      width: 100
    },
    {
      field: 'tol',
      headerName: 'Tolerance',
      editable: true,
      width: 150,
      validate: isValidFloat
    },
    {
      field: 'relTol',
      headerName: 'Relative',
      editable: true,
      width: 150,
      validate: isValidFloat
    }
  ]

  const edits = []
  for (const row of rows) {
    edits.push({
      foamObj: solversToken.get(row.var).get('tolerance').get(0),
      newValue: row.tol
    })
    edits.push({
      foamObj: solversToken.get(row.var).get('relTol').get(0),
      newValue: row.relTol
    })
  }
  function validate() {
    return (
      rows.map((row) => row.tol).every(isValidFloat) &&
      rows.map((row) => row.relTol).every(isValidFloat)
    )
  }
  return (
    <>
      <h3>Convergence Criteria</h3>
      <Styled.Grid gap="1em">
        <CustomTable
          width={350}
          rows={rows}
          columns={columns}
          setRows={setRows}
        />
        <Save edits={edits} isValid={validate()} />
      </Styled.Grid>
    </>
  )
}
