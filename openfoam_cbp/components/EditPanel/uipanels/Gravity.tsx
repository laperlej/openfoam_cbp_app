import { useState } from 'react'
import { useAst } from 'components/@shared/hooks/useAst'
import { isValidFloat } from './utils/validators'
import * as Styled from 'components/@shared/styles'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { Save } from './utils/Save'

export const Gravity = ({ filePath }) => {
  const ast = useAst(filePath)
  const gravityToken = ast.get('value').get(0).get(1)
  const [gravity, setGravity] = useState(gravityToken.value || '0')
  const edits = [{ foamObj: gravityToken, newValue: gravity }]

  const validate = () => {
    return isValidFloat(gravity)
  }

  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="0.5em" />
      <div>
        <Styled.BlockSpan>Set gravitational force.</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '120px' }}
          value={gravity}
          validationFn={isValidFloat}
          onChange={(event) => setGravity(event.target.value)}
          data-testid="gravity-input"
        />
      </div>
      <Save edits={edits} isValid={validate()} />
    </Styled.Grid>
  )
}
