import React, { useState } from 'react'
import { Save } from './utils/Save'
import { isValidFloat, isValidInt } from './utils/validators'
import { CustomTable } from 'components/@ui/CustomTable'
import { range } from './utils/range'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../@shared/hooks/useAst'

const materialChoices = [
  'AcrylicPaint',
  'BuildingPaper',
  'GlassFiberInsulation',
  'GypsumBoard',
  'Hamstad1Concrete',
  'Hamstad1Insulation',
  'Hamstad3Wall',
  'Hamstad4Brick',
  'Hamstad4Plaster',
  'Hamstad5Brick',
  'Hamstad5Insulation',
  'Hamstad5Mortar',
  'Masonry',
  'OSB',
  'SBPO',
  'Soil',
  'VanGenuchten',
  'VanGenuchtenVapDiff',
  'VWC'
]

function init_rows(ast) {
  const buildingMaterialToken = ast.get('buildingMaterials').get(0)

  const nbLayers = buildingMaterialToken.value?.length || 0

  const getAttributeValues = (attributeName) =>
    buildingMaterialToken.value?.map(
      (materialObj) => materialObj.get(attributeName).get(0).value
    ) || Array(10).fill(0)

  const materials = getAttributeValues('buildingMaterialModel')
  const rho = getAttributeValues('rho')
  const cap = getAttributeValues('cap')
  const l1 = getAttributeValues('lambda1')
  const l2 = getAttributeValues('lambda2')

  return range(1, nbLayers + 1).map((i) => {
    return {
      id: i,
      material: materials[i - 1],
      rho: rho[i - 1],
      cap: cap[i - 1],
      l1: l1[i - 1],
      l2: l2[i - 1]
    }
  })
}

export const HamTransportProperties = ({ filePath }) => {
  const ast = useAst(filePath)
  const [rows, setRows] = useState(init_rows(ast))
  const columns = [
    {
      field: 'id',
      headerName: 'Layer',
      editable: false,
      width: 50
    },
    {
      field: 'material',
      headerName: 'Material',
      editable: true,
      width: 170,
      options: materialChoices,
      foamName: 'buildingMaterialModel'
    },
    {
      field: 'rho',
      headerName: 'Rho',
      editable: true,
      width: 70,
      validate: isValidInt,
      foamName: 'rho'
    },
    {
      field: 'cap',
      headerName: 'Cap',
      editable: true,
      width: 70,
      validate: isValidInt,
      foamName: 'cap'
    },
    {
      field: 'l1',
      headerName: 'Lambda1',
      editable: true,
      width: 80,
      validate: isValidFloat,
      foamName: 'lambda1'
    },
    {
      field: 'l2',
      headerName: 'Lambda2',
      editable: true,
      width: 80,
      validate: isValidFloat,
      foamName: 'lambda2'
    }
  ]

  const edits = []
  for (let i = 0; i < rows.length; i++) {
    for (let c = 1; c < columns.length; c++) {
      edits.push({
        foamObj: ast
          .get('buildingMaterials')
          .get(0)
          .get(i)
          .get(columns[c].foamName)
          .get(0),
        newValue: rows[i][columns[c].field]
      })
    }
  }

  function validate() {
    return (
      rows.map((row) => row.rho).every(isValidInt) &&
      rows.map((row) => row.cap).every(isValidInt) &&
      rows.map((row) => row.l1).every(isValidFloat) &&
      rows.map((row) => row.l2).every(isValidFloat)
    )
  }

  return (
    <div data-testid="ham-transport-properties">
      <h3>Materials configuration</h3>
      <Styled.Grid gap="1em">
        <CustomTable rows={rows} columns={columns} setRows={setRows} />
        <Save edits={edits} isValid={validate()} />
      </Styled.Grid>
    </div>
  )
}
