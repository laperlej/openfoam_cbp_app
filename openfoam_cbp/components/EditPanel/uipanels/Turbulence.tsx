import React, { useState } from 'react'
import { Save } from './utils/Save'
import { CustomSelect } from 'components/@ui/CustomSelect'
import FormControl from '@mui/material/FormControl'
import * as Styled from 'components/@shared/styles'
import { useAst } from '../../@shared/hooks/useAst'

const rasModels = [
  'LRR',
  'LaunderSharmaKE',
  'RNGkEpsilon',
  'SSG',
  'SpalartAllmaras',
  'buoyantKEpsilon',
  'kEpsilon',
  'kOmega',
  'kOmegaSST',
  'kOmegaSSTLM',
  'kOmegaSSTSAS',
  'porousrealizableKE',
  'realizableKE',
  'v2f'
]

export const Turbulence = ({ filePath }) => {
  const ast = useAst(filePath)
  const rasModelToken = ast.get('RAS').get('RASModel').get(0)
  const [rasModel, setRasModel] = useState<string>(
    (rasModelToken.value as string) || 'kEpsilon'
  )
  const edits = [{ foamObj: rasModelToken, newValue: rasModel }]

  function validate() {
    return true
  }
  return (
    <>
      <h3>RAS Model</h3>
      <Styled.BlockSpan>
        note: this may require changes to the fvschemes file.
      </Styled.BlockSpan>
      <Styled.Grid gap="1em">
        <FormControl sx={{ m: 1, minWidth: 150 }} size="small">
          <CustomSelect
            style={{ width: '200px' }}
            options={rasModels}
            onChange={(event) => setRasModel(event.target.value)}
            value={rasModel}
          />
        </FormControl>
        <Save edits={edits} isValid={validate()} />
      </Styled.Grid>
    </>
  )
}
