import { useState } from 'react'
import FormControl from '@mui/material/FormControl'
import { useAst } from 'components/@shared/hooks/useAst'
import { isValidFloat } from './utils/validators'
import * as Styled from 'components/@shared/styles'
import { CustomTextField } from 'components/@ui/CustomTextField'
import { Save } from './utils/Save'
import { CustomSelect } from 'components/@ui/CustomSelect'

const solveTdOptions = ['yes', 'no']

export const WindTransport = ({ filePath }) => {
  const ast = useAst(filePath)
  const solveTdToken = ast.get('solveTD').get(0)
  const scalingFactorToken = ast.get('scalingFactor').get(0)
  const [solveTd, setSolveTd] = useState<string>(
    (solveTdToken.value as string) || 'no'
  )
  const [scalingFactor, setScalingFactor] = useState(
    scalingFactorToken.value || '0'
  )

  const edits = [
    { foamObj: solveTdToken, newValue: solveTd },
    { foamObj: scalingFactorToken, newValue: scalingFactor }
  ]

  const validate = () => {
    return isValidFloat(scalingFactor)
  }

  return (
    <Styled.Grid gap="1em">
      <Styled.Spacer size="0.5em" />
      <div>
        <Styled.BlockSpan>SolveTD</Styled.BlockSpan>
        <FormControl sx={{ m: 1, minWidth: 100 }} size="small">
          <CustomSelect
            style={{ width: '100px' }}
            options={solveTdOptions}
            onChange={(event) => setSolveTd(event.target.value)}
            value={solveTd}
            data-testid="solvetd-select"
          />
        </FormControl>
      </div>
      <div>
        <Styled.BlockSpan>Set scaling factor.</Styled.BlockSpan>
        <CustomTextField
          style={{ width: '100px' }}
          value={scalingFactor}
          validationFn={isValidFloat}
          onChange={(event) => setScalingFactor(event.target.value)}
          data-testid="scaling-factor-input"
        />
      </div>
      <Save edits={edits} isValid={validate()} />
    </Styled.Grid>
  )
}
