import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { TestCaseContext } from 'components/@shared/testUtils/TestCaseContext'
import { AblConditions } from '../AblConditions'

describe('AblConditions', () => {
  it('Defaults', async () => {
    render(
      <TestCaseContext solverName={'urbanMicroclimateFoam'}>
        <AblConditions />
      </TestCaseContext>
    )
    const uRefInput = screen.getByTestId('uRef-input')
    expect(uRefInput).toHaveValue('5.0')
    const zRefInput = screen.getByTestId('zRef-input')
    expect(zRefInput).toHaveValue('10.0')
    const z0Input = screen.getByTestId('z0-input')
    expect(z0Input).toHaveValue('0.03')
    const zGroundInput = screen.getByTestId('zGround-input')
    expect(zGroundInput).toHaveValue('0.0')
    const flowDirXInput = screen.getByTestId('flowDirX-input')
    expect(flowDirXInput).toHaveValue('1')
    const flowDirYInput = screen.getByTestId('flowDirY-input')
    expect(flowDirYInput).toHaveValue('0')
    const flowDirZInput = screen.getByTestId('flowDirZ-input')
    expect(flowDirZInput).toHaveValue('0')
  })
})
