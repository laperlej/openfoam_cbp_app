import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { AutoUIPanel } from 'components/@shared/testUtils/AutoUIPanel'

describe('Gravity', () => {
  it('Default is -9.81', async () => {
    render(
      <AutoUIPanel
        solverName={'windDrivenRainFoam'}
        filePath={'windDrivenRainFoam/constant/g'}
      />
    )

    const gravityInput = screen.getByTestId('gravity-input')
    expect(gravityInput).toHaveValue('-9.81')
  })
})
