import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { AutoUIPanel } from 'components/@shared/testUtils/AutoUIPanel'

describe('wind transport', () => {
  it('Default for solvetd is no and scaling factor is 0.5', async () => {
    render(
      <AutoUIPanel
        solverName={'windDrivenRainFoam'}
        filePath={'windDrivenRainFoam/constant/transportProperties'}
      />
    )
    const scalingInput = screen.getByTestId('scaling-factor-input')
    const solvetdDropdown = screen.getByTestId('solvetd-select')
    expect(scalingInput).toHaveValue('0.5')
    expect(solvetdDropdown).toHaveValue('no')
  })
})
