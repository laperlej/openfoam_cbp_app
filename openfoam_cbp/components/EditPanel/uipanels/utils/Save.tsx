import React, { useState, useRef, useContext } from 'react'
import Button from '@mui/material/Button'
import { useMonaco, Monaco } from '@monaco-editor/react'
import { EditorContext } from 'components/@shared/contexts/EditorContext'
import { CustomAlert } from 'components/@ui/CustomAlert'
import * as Styled from 'components/@shared/styles'

class Highlighter {
  monaco: Monaco | null
  editor: monaco.editor.IStandaloneCodeEditor | null
  newDecorations: monaco.editor.IModelDeltaDecoration[]
  minLine: number
  oldDecorations: { current: string[] }
  constructor(
    monaco: Monaco | null,
    editor: monaco.editor.IStandaloneCodeEditor | null,
    oldDecorations: { current: string[] }
  ) {
    this.monaco = monaco
    this.editor = editor
    this.newDecorations = []
    this.minLine = Number.MAX_SAFE_INTEGER
    this.oldDecorations = oldDecorations
  }
  push(
    foamObj: {
      value: string
      range: [number, number, number, number]
    },
    newValue: string
  ) {
    if (foamObj.value != newValue) {
      const range = new this.monaco.Range(...foamObj.range)
      this.newDecorations.push({
        range: range,
        options: { inlineClassName: 'highlightMonaco' }
      })
      this.minLine = Math.min(this.minLine, range.startLineNumber)
    }
  }
  highlight() {
    this.oldDecorations.current = this.editor.deltaDecorations(
      this.oldDecorations.current,
      this.newDecorations
    )
    if (this.minLine < Number.MAX_SAFE_INTEGER)
      this.editor.revealLineInCenter(this.minLine)
    this.newDecorations = []
    return this.oldDecorations.current.length == 0
  }
}

class EditMaker {
  monaco: Monaco | null
  editor: monaco.editor.IStandaloneCodeEditor | null
  edits: { range: monaco.Range; text: string }[]
  constructor(
    monaco: Monaco | null,
    editor: monaco.editor.IStandaloneCodeEditor | null
  ) {
    this.monaco = monaco
    this.editor = editor
    this.edits = []
  }
  push(
    foamObj: { value: string; range: [number, number, number, number] },
    newValue: string
  ) {
    if (foamObj.value != newValue) {
      const range: monaco.Range = new this.monaco.Range(...foamObj.range)
      this.edits.push({ range: range, text: newValue })
    }
  }
  edit() {
    this.editor.executeEdits('', this.edits)
    this.edits = []
  }
}

export const Save = ({ edits, isValid = true, onClick = (e) => null }) => {
  const [warn, setWarn] = useState(false)
  const oldDecorations = useRef([])
  const monaco = useMonaco()
  const editor = useContext(EditorContext)
  const highlighter = new Highlighter(monaco, editor, oldDecorations)
  const editMaker = new EditMaker(monaco, editor)
  const save = () => {
    for (const i in edits) {
      if (edits[i].foamObj) {
        editMaker.push(edits[i].foamObj, edits[i].newValue)
        highlighter.push(edits[i].foamObj, edits[i].newValue)
      }
    }
    setWarn(highlighter.highlight())
    editMaker.edit()
    onClick(edits)
  }
  return (
    <Styled.BlockDiv>
      <Button
        disabled={isValid ? false : true}
        onClick={save}
        variant="contained"
        data-testid="edit-save"
      >
        Save
      </Button>
      <CustomAlert
        open={warn}
        onClose={() => setWarn(false)}
        severity="info"
        data-testid="no-changes-alert"
      >
        No changes needed.
      </CustomAlert>
    </Styled.BlockDiv>
  )
}
