export const HelpMessage = () => {
  return (
    <span data-testid="edit-section-message">
      <h4>Edit Section</h4>
      Select files on the right hand panel to adjust the case&apos;s settings.
      <br />
      Files in the <b>system</b> folder are a good place to start.
    </span>
  )
}
