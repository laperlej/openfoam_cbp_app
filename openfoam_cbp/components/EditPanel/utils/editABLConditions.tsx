import { Parser } from 'components/@shared/utils/Parser'
import { Token } from 'components/@shared/utils/Parser/Token'

export function getABLPath(solverName) {
  return solverName === 'urbanMicroclimateFoam'
    ? '0/air/include/ABLConditions'
    : 'simpleFoam/0/include/ABLConditions'
}

function clearABLmodel(monaco, index) {
  for (const model of monaco.editor.getModels()) {
    if (`/${index}` == model.uri.path) {
      model.dispose()
    }
  }
}

function getEdit(token, newValue, monaco) {
  const range = new monaco.Range(...token.range)
  return { range: range, text: newValue }
}

function prepareEdits(text, newValues, monaco) {
  const ablAst = new Parser().parse(text)
  const tokenMap = getTokenMap(ablAst)
  const varNames: string[] = Array.from(newValues.keys())
  return varNames.map((varName) =>
    getEdit(tokenMap.get(varName), newValues.get(varName), monaco)
  )
}

function editABL(monaco, fileInfo, edits): string {
  const model = monaco.editor.createModel(fileInfo.text)
  console.log(edits)
  model.applyEdits(edits)
  const newFileText = model.getValue()
  model.dispose()
  return newFileText
}

function getTokenMap(ast) {
  return new Map<string, Token>([
    ['uRef', ast.get('Uref').get(0)],
    ['zRef', ast.get('Zref').get(0)],
    ['flowDirX', ast.get('flowDir').get(0).get(0)],
    ['flowDirY', ast.get('flowDir').get(0).get(1)],
    ['flowDirZ', ast.get('flowDir').get(0).get(2)],
    ['zGround', ast.get('zGround').get(1)],
    ['z0', ast.get('z0').get(1)]
  ])
}

export function editABLConditions(
  newValues: Map<string, string>,
  fileInfo,
  monaco
): string {
  clearABLmodel(monaco, fileInfo.path)
  const edits = prepareEdits(fileInfo.text, newValues, monaco)
  return editABL(monaco, fileInfo, edits)
}
