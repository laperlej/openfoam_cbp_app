export function editMaterials(
  segmentXCoordinates,
  setInfo,
  transportInfo,
  monaco
) {
  for (const model of monaco.editor.getModels()) {
    if (
      `/${setInfo.path}` === model.uri.path ||
      `/${transportInfo.path}` === model.uri.path
    ) {
      model.dispose()
    }
  }
  const newSetSet = []
  for (let i = 0; i < segmentXCoordinates.length - 1; ++i) {
    newSetSet.push(
      `cellSet layer${i + 1} new boxToCell (${segmentXCoordinates[i]} 0.00 0)(${
        segmentXCoordinates[i + 1]
      } 1 1)`
    )
    newSetSet.push(`cellZoneSet layer${i + 1} new setToCellZone layer${i + 1}`)
  }

  const newTransportProperties = [
    '/*--------------------------------*- C++ -*----------------------------------*\\\n| =========                 |                                                 |\n| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n|  \\\\    /   O peration     | Version:  2.2.2                                 |\n|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n|    \\\\/     M anipulation  |                                                 |\n\\*---------------------------------------------------------------------------*/\nFoamFile\n{\n    version     2.0;\n    format      ascii;\n    class       dictionary;\n    location    "constant";\n    object      transportProperties;\n}\n// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\nbuildingMaterials\n('
  ]
  for (let i = 0; i < segmentXCoordinates.length - 1; ++i) {
    newTransportProperties.push(
      `    {\n        name	layer${
        i + 1
      };\n        buildingMaterialModel Hamstad5Brick;\n        rho     1600;\n        cap     1000;\n        lambda1	0.682;	//lambda = lambda1 + ws*lambda2\n        lambda2	0;\n    }`
    )
  }
  newTransportProperties.push(
    ');\n\n// ************************************************************************* //'
  )
  return {
    newSetText: newSetSet.join('\n'),
    newTransportText: newTransportProperties.join('\n')
  }
}
