export function isBash(fileName) {
  return [
    'Allclean',
    'Allrun',
    'Allprepare',
    'reconstructScript',
    'setset.batch',
    'buildings.obj'
  ].includes(fileName)
}
