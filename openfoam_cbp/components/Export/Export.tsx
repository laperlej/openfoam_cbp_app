import { useState } from 'react'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import * as Styled from 'components/@shared/styles'
import axios from 'axios'
import CircularProgress from '@mui/material/CircularProgress'
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline'
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined'
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'

const callCaseExport = async (caseFiles, solverName, setStatus) => {
  setStatus('loading')
  try {
    await axios.post(`/api/export/prepare`, {
      caseFiles: caseFiles,
      solver: solverName
    })
    setStatus('ready')
  } catch (error) {
    setStatus('error')
  }
}

export const Export = () => {
  const { state } = useCaseContext()
  const [open, setOpen] = useState(false)
  const [status, setStatus] = useState('loading')

  const handleClick = () => {
    setOpen(true)
    callCaseExport(state.caseFiles, state.solverName, setStatus)
  }
  const handleDownload = () => {
    window.location.href = `/api/export/download`
  }
  return (
    <>
      <Styled.ImportExportButton
        disabled={state.solverName === null}
        onClick={handleClick}
        variant="contained"
      >
        Export
      </Styled.ImportExportButton>
      <ExportDialog
        open={open}
        setOpen={setOpen}
        status={status}
        onRetry={handleClick}
        onDownload={handleDownload}
      />
    </>
  )
}

const ExportDialog = ({ open, setOpen, status, onRetry, onDownload }) => {
  return (
    <Dialog open={open} onClose={() => setOpen(false)}>
      <Styled.DialogBox>
        <DialogTitle>Export</DialogTitle>
        {status === 'loading' && <ExportLoading />}
        {status === 'ready' && <ExportReady onClick={onDownload} />}
        {status === 'error' && <ExportError onClick={onRetry} />}
      </Styled.DialogBox>
    </Dialog>
  )
}

const ExportReady = ({ onClick }) => {
  return (
    <Styled.GridContent>
      <Styled.VCenterBox>
        <CheckCircleOutlineIcon color="success" />
        Your files are ready!
        <br />
      </Styled.VCenterBox>
      <Styled.CenterBox>
        <Styled.DownloadButton onClick={onClick} variant="contained">
          Download
        </Styled.DownloadButton>
      </Styled.CenterBox>
    </Styled.GridContent>
  )
}

const ExportLoading = () => {
  return (
    <Styled.GridContent>
      <Styled.VCenterBox>
        <InfoOutlinedIcon color="primary" />
        Preparing your files
        <br />
      </Styled.VCenterBox>
      <Styled.CenterBox>
        <CircularProgress />
      </Styled.CenterBox>
    </Styled.GridContent>
  )
}

const ExportError = ({ onClick }) => {
  return (
    <Styled.GridContent>
      <Styled.VCenterBox>
        <ErrorOutlineIcon color="error" />
        Failed to prepare your files
        <br />
      </Styled.VCenterBox>
      <Styled.CenterBox>
        <Styled.DownloadButton onClick={onClick} variant="contained">
          Retry
        </Styled.DownloadButton>
      </Styled.CenterBox>
    </Styled.GridContent>
  )
}
