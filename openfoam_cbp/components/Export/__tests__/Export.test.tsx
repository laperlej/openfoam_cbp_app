import axios from 'axios'
import { TestCaseContext } from 'components/@shared/testUtils/TestCaseContext'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Export } from 'components/Export'
import userEvent from '@testing-library/user-event'
import { getCaseFiles } from 'server/case/caseFiles'

jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

describe('Export Dialog', () => {
  test('disbled on empty context', async () => {
    render(
      <TestCaseContext solverName={null}>
        <Export />
      </TestCaseContext>
    )
    const exportBtn = screen.getByRole('button', { name: 'Export' })
    expect(exportBtn).toBeDisabled()
  })
  test('Conditional render', async () => {
    const testState = {
      solver: 'hamFoam',
      caseFiles: getCaseFiles('hamFoam')
    }
    const apiAddress = '/api/export/prepare'
    const user = userEvent.setup()
    render(
      <TestCaseContext>
        <Export />
      </TestCaseContext>
    )
    //button exists
    const exportBtn = screen.getByRole('button', { name: 'Export' })
    expect(exportBtn).toBeEnabled()
    //click to open dialog
    mockedAxios.post.mockRejectedValueOnce({ status: 500 })
    await user.click(exportBtn)
    expect(axios.post).toHaveBeenCalledWith(apiAddress, testState)
    const exportDialog = screen.getByRole('dialog', { name: 'Export' })
    expect(exportDialog).toBeInTheDocument()
    //retry button appears on error status
    const retryBtn = screen.getByRole('button', { name: 'Retry' })
    expect(retryBtn).toBeInTheDocument()
    //download button appears if success status
    mockedAxios.post.mockResolvedValueOnce({ status: 200 })
    await user.click(retryBtn)
    expect(axios.post).toHaveBeenCalledWith(apiAddress, testState)
    const downloadBtn = screen.getByRole('button', { name: 'Download' })
    expect(downloadBtn).toBeInTheDocument()
  })
})
