import React from 'react'
import HelpMdx from './help.mdx'
import * as Styled from './styles'

export const HelpPanel = () => {
  return (
    <Styled.HelpMargin>
      <HelpMdx />
    </Styled.HelpMargin>
  )
}
