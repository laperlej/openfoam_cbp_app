import styled from '@emotion/styled'

export const HelpMargin = styled.div`
  margin: 10px 50px 50px 50px;
  padding-bottom: 60px;
`
