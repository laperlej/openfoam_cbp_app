import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import * as Styled from 'components/@shared/styles'
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline'
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'
import { Upload } from 'components/SolverPanel/Upload'
import styled from '@emotion/styled'
import { useMonaco } from '@monaco-editor/react'

const UploadBox = styled.div`
  text-align: center;
`

export const Import = () => {
  const { state } = useCaseContext()
  const [open, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(true)
  }
  return (
    <>
      <Styled.ImportExportButton
        disabled={state.solverName === null}
        onClick={handleClick}
        variant="contained"
      >
        Import
      </Styled.ImportExportButton>
      <ImportDialog open={open} setOpen={setOpen} />
    </>
  )
}

const ImportDialog = ({ open, setOpen }) => {
  const { state, dispatch } = useCaseContext()
  const [status, setStatus] = useState('Not Started')
  const monaco = useMonaco()
  const router = useRouter()

  const uploadFinishHandler = () => {
    if (status === 'UploadStarted') {
      setStatus('Upload Complete')
    }
  }
  const uploadStartHandler = () => {
    setStatus('Upload Started')
  }
  const closeDialog = () => {
    setOpen(false)
  }
  const handleRetry = () => {
    setStatus('Not Started')
  }
  function clearMonaco() {
    if (monaco) monaco.editor.getModels().forEach((model) => model.dispose())
  }
  function newCase(solver, caseFiles, objFileName) {
    dispatch(state, {
      solver: solver,
      caseFiles: caseFiles || {
        root: {
          index: 'root',
          data: 'Root item',
          hasChildren: true,
          children: [],
          text: ''
        }
      },
      objFileName: objFileName
    })
    localStorage.setItem('solverName', solver)
    localStorage.setItem('objFileName', objFileName)
    localStorage.setItem('caseFiles', JSON.stringify(caseFiles))
    clearMonaco()
  }

  function handleUploadResponse(response) {
    if (response.status !== 200) {
      setStatus('Error')
      return
    }
    setStatus('Success')
    const objFileName =
      response.data.objFileName !== '' ? response.data.objFileName : null
    newCase(response.data.solver, response.data.caseFiles, objFileName)
    router.reload()
  }
  let dialogContent = (
    <UploadBox>
      <Upload
        title="Import a case file."
        open={open}
        accept=".tar.gz"
        handleResponse={handleUploadResponse}
        onUploadFinish={uploadFinishHandler}
        onUploadStart={uploadStartHandler}
        endpoint="/api/import"
      />
    </UploadBox>
  )
  if (status === 'Success') {
    dialogContent = <ImportSuccess onClick={closeDialog} />
  } else if (status === 'Error') {
    dialogContent = <ImportError onClick={handleRetry} />
  }

  return (
    <Dialog open={open} onClose={() => setOpen(false)}>
      <Styled.ImportDialogBox>
        <DialogTitle>Import</DialogTitle>
        <Styled.GridContent>{dialogContent}</Styled.GridContent>
      </Styled.ImportDialogBox>
    </Dialog>
  )
}

const ImportError = ({ onClick }) => {
  return (
    <>
      <Styled.VCenterBox>
        <ErrorOutlineIcon color="error" />
        Failed to import your file
        <br />
      </Styled.VCenterBox>
      <Styled.CenterBox>
        <Styled.DownloadButton onClick={onClick} variant="contained">
          Retry
        </Styled.DownloadButton>
      </Styled.CenterBox>
    </>
  )
}

const ImportSuccess = ({ onClick }) => {
  return (
    <>
      <Styled.VCenterBox>
        <CheckCircleOutlineIcon color="success" />
        Import successful
        <br />
      </Styled.VCenterBox>
      <Styled.CenterBox>
        <Styled.DownloadButton onClick={onClick} variant="contained">
          Close
        </Styled.DownloadButton>
      </Styled.CenterBox>
    </>
  )
}
