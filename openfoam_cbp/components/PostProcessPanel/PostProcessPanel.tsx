import React, { useState, useEffect, useRef, useCallback } from 'react'
import {
  Token,
  tokenType,
  LexToken
} from 'components/@shared/utils/Parser/Token'
import axios from 'axios'
import { Parser } from 'components/@shared/utils/Parser'
import Switch from '@mui/material/Switch'
import { CustomSelect } from '../@ui/CustomSelect'
import LoadingButton from '@mui/lab/LoadingButton'
import { CustomTextField } from '../@ui/CustomTextField'
import { isValidTime } from 'components/EditPanel/uipanels/utils/validators'
import { CustomMultiSelect } from 'components/@ui/CustomMultiSelect'
import EventSource from 'eventsource'
import Box from '@mui/material/Box'
import { RunStatusMessage } from './statusMessages/RunStatusMessage'
import { PostStatusMessage } from './statusMessages/PostStatusMessage'
import * as Styled from 'components/@shared/styles'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'

const getFieldOptions = (solverName) => {
  switch (solverName) {
    case 'hamFoam':
      return ['pc', 'Ts', 'ws']
    case 'urbanMicroclimateFoam':
      return [
        'alphat',
        'epsilon',
        'gcr',
        'k',
        'nut',
        'p',
        'p_rgh',
        'T',
        'U',
        'w'
      ]
    case 'windDrivenRainFoam':
      return ['epsilon', 'k', 'U', 'nut', 'p']
    default:
      return []
  }
}

function getRegionOptions(caseFiles) {
  try {
    const parser = new Parser()
    const ast = parser.parse(caseFiles['constant/regionProperties']['text'])
    const meshRegions: Array<string> = []
    const tokens = ast?.get('regions')?.get(0)
    for (const i in tokens.value as unknown as Array<Token>) {
      if (tokens.get(i).type === tokenType.Array) {
        const regionTokens = tokens.get(i).value as unknown as Array<LexToken>
        for (const lexToken of regionTokens) {
          meshRegions.push(lexToken.value as string)
        }
      }
    }
    return meshRegions
  } catch (error) {
    return []
  }
}

export const PostProcessPanel = () => {
  console.log('HELLO')
  const { state } = useCaseContext()
  const [runStatus, setRunStatus] = useState('unknown')
  const [postStatus, setPostStatus] = useState('unknown')
  const [fields, setFields] = useState([])
  const [region, setRegion] = useState('')
  const [times, setTimes] = useState('')
  const [allPatches, setAllPatches] = useState(false)
  const statusSource: React.MutableRefObject<EventSource | null> = useRef(null)
  //alerts
  const [showAlert, setShowAlert] = useState(true)
  useEffect(() => {
    setShowAlert(true)
  }, [runStatus, postStatus])

  //SSE
  const listener = useCallback(
    (event) => {
      switch (event.type) {
        case 'runStatus':
          setRunStatus(event.data)
          break
        case 'postStatus':
          setPostStatus(event.data)
          break
        case 'end':
          statusSource.current.close()
          break
      }
    },
    [statusSource]
  )

  const setListeners = useCallback(() => {
    if (statusSource.current) {
      statusSource.current.addEventListener('runStatus', listener)
      statusSource.current.addEventListener('postStatus', listener)
      statusSource.current.addEventListener('end', listener)
    }
  }, [statusSource, listener])

  const initEventSource = useCallback(() => {
    setRunStatus('unknown')
    statusSource.current = new EventSource(
      new URL('/api/status', window.location.origin).href
    )
    statusSource.current.onerror = (err) => {
      statusSource.current.close()
      if (err.status === 401) {
        setRunStatus('no session')
        return
      }
      if (err.status === 400 || err.status === 502) {
        setRunStatus('no connection')
        return
      }
      console.log(err.message)
      if (err.message == 'Error in body stream') {
        return
      }
      setRunStatus('1')
    }
    setListeners()
  }, [setListeners, setRunStatus])

  useEffect(() => {
    initEventSource()
    return () => statusSource.current?.close()
  }, [initEventSource])

  //postprocess
  const runPostprocess = () => {
    async function postPostprocess() {
      try {
        await axios.post('/api/postprocess', {
          fields: fields,
          region: region,
          times: times,
          allPatches: allPatches
        })
      } catch (err) {
        if (err.response.status === 401) {
          setPostStatus('no session')
          setRunStatus('no session')
          return
        }
        if (err.response.status === 400 || err.response.status === 502) {
          setPostStatus('no connection')
          setRunStatus('no connection')
          return
        }
        setPostStatus('1')
      }
    }
    setPostStatus('running')
    postPostprocess()
  }

  const toggleAllPatches = () => {
    setAllPatches(!allPatches)
  }

  return (
    <Styled.CenteredText>
      <h2 style={{ marginTop: '20px' }}>Postprocessing Options</h2>
      <Styled.Grid gap="1.5em">
        <div>
          <Styled.BlockSpan>
            Select one or many fields to include (optional)
          </Styled.BlockSpan>
          <Styled.BlockSpan style={{ color: '#CCCCCC' }}>
            Leave empty to include all fields
          </Styled.BlockSpan>
          <CustomMultiSelect
            value={fields}
            label="Fields"
            options={getFieldOptions(state.solverName)}
            onChange={(event) => setFields(event.target.value as string[])}
          />
        </div>
        {state.caseFiles['constant/regionProperties'] && (
          <div>
            <Styled.BlockSpan>
              Specify alternative mesh region (optional)
            </Styled.BlockSpan>
            <CustomSelect
              style={{ width: '200px' }}
              onChange={(event) => setRegion(event.target.value)}
              label={'Region'}
              options={getRegionOptions(state.caseFiles)}
            />
          </div>
        )}
        <div>
          <Styled.BlockSpan>
            Select which times to include (optional)
          </Styled.BlockSpan>
          <Styled.BlockSpan style={{ color: '#CCCCCC' }}>
            Leave empty to include all times
          </Styled.BlockSpan>
          <Styled.BlockSpan style={{ color: '#CCCCCC' }}>
            comma-separated time ranges - eg, &apos;0:10,20,40:70,1000:&apos;
          </Styled.BlockSpan>
          <CustomTextField
            value={times}
            style={{ width: '200px' }}
            label={'Times'}
            validationFn={isValidTime}
            onChange={(event) => setTimes(event.target.value)}
          />
        </div>
        <div>
          <Styled.BlockSpan>
            Combine all patches into a single file
          </Styled.BlockSpan>
          <Switch onChange={toggleAllPatches} />
        </div>
        <LoadingButton
          variant="contained"
          onClick={runPostprocess}
          loading={runStatus == 'running'}
          disabled={runStatus !== '0' || !isValidTime(times)}
          data-testid="post-start"
        >
          Run Postprocess
        </LoadingButton>
        <Box
          sx={{ display: 'inline-block', margin: 'auto', textAlign: 'center' }}
        >
          <PostStatusMessage postStatus={postStatus} />
        </Box>
      </Styled.Grid>
      <RunStatusMessage
        open={showAlert}
        onClose={() => setShowAlert(false)}
        runStatus={runStatus}
        postStatus={postStatus}
      />
    </Styled.CenteredText>
  )
}
