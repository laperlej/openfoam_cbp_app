import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import PostProcess from 'pages/postprocess'

describe('PostprocessPanel', () => {
  it('renders with empty context', () => {
    render(<PostProcess />)
  })
  it('alerts me about unkown status', () => {
    render(<PostProcess />)
    expect(screen.getByTestId('runstatus-unknown')).toBeInTheDocument()
    expect(screen.queryByTestId('poststatus-error')).not.toBeInTheDocument()
  })
})
