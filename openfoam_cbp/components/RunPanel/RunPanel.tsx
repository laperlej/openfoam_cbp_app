import React, { useState } from 'react'
import axios from 'axios'
import LoadingButton from '@mui/lab/LoadingButton'
import Switch from '@mui/material/Switch'
import { useRouter } from 'next/router'
import { CustomAlert } from 'components/@ui/CustomAlert'
import * as Styled from 'components/@shared/styles'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'

export const RunPanel = () => {
  const { state } = useCaseContext()
  const [multiProcessing, setMultiProcessing] = useState(false)
  const [latestTime, setLatestTime] = useState(true)
  const [sendingCaseStatus, setSendingCaseStatus] = useState('notStarted')
  const router = useRouter()
  const runSolver = () => {
    function postCase() {
      setSendingCaseStatus('waiting')
      axios
        .post('/api/runcase', {
          caseFiles: state.caseFiles,
          multiProcessing: multiProcessing,
          latestTime: latestTime
        })
        .then(() => {
          router.push('/log')
        })
        .catch((err) => {
          if (err.response.status === 401) {
            setSendingCaseStatus('noSession')
            return
          }
          setSendingCaseStatus('error')
        })
    }
    postCase()
  }
  const onMultiProcessingToggle = () => {
    setMultiProcessing(!multiProcessing)
  }
  const onLatestTimeToggle = () => {
    setLatestTime(!latestTime)
  }
  return (
    <Styled.CenteredText>
      <h3>Start the solver</h3>
      <Styled.Grid gap="1em">
        <div>
          <Styled.BlockSpan>Use multiple processors</Styled.BlockSpan>
          <Switch onChange={onMultiProcessingToggle} />
        </div>
        {multiProcessing && (
          <div>
            <Styled.BlockSpan>
              Reconstruct only the lastest time
            </Styled.BlockSpan>
            <Switch defaultChecked onChange={onLatestTimeToggle} />
            <Styled.BlockSpan>
              <b>Important</b>:{' '}
              <i>
                Be sure to adjust the decomposeParDict file(s) in the system
                folder(s).
              </i>
            </Styled.BlockSpan>
          </div>
        )}
        <LoadingButton
          loading={sendingCaseStatus === 'waiting' ? true : false}
          disabled={state.solverName ? false : true}
          onClick={runSolver}
          variant="contained"
          data-testid="run-start"
        >
          Run and view logs
        </LoadingButton>
      </Styled.Grid>
      <CustomAlert
        open={
          sendingCaseStatus === 'error' || sendingCaseStatus === 'noSession'
        }
        onClose={() => setSendingCaseStatus('notStarted')}
        severity="error"
      >
        {sendingCaseStatus === 'error'
          ? 'Failed to connect to the server. Please try again.'
          : 'No session found. Please start over from the solver section'}
      </CustomAlert>
    </Styled.CenteredText>
  )
}
