import Box from '@mui/material/Box'
import LinearProgress from '@mui/material/LinearProgress'

export const Progress = ({ progress }) => {
  const progressMessage = progress == 100 ? 'Upload Completed' : 'Uploading...'
  return (
    <Box
      sx={{
        width: '200px',
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
      }}
    >
      <LinearProgress variant="determinate" value={progress} />
      {progressMessage}
    </Box>
  )
}
