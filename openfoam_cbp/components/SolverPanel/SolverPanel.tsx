import React, { useState, useContext } from 'react'
import { SelectChangeEvent } from '@mui/material/Select'
import { SolverSelect } from './SolverSelect'
import { Upload } from './Upload'
import axios from 'axios'
import LoadingButton from '@mui/lab/LoadingButton'
import { useRouter } from 'next/router'
import { CaseContext } from 'components/@shared/contexts/CaseContext'
import { useMonaco } from '@monaco-editor/react'
import { CustomAlert } from 'components/@ui/CustomAlert'
import * as Styled from 'components/@shared/styles'

export const SolverPanel = () => {
  const { dispatch } = useContext(CaseContext)
  const [solver, setSolver] = useState('hamFoam')
  const [objFile, setObjFile] = useState(null)
  const [isUploading, setIsUploading] = useState(false)
  const [sendingCaseStatus, setSendingCaseStatus] = useState('notStarted')
  const router = useRouter()
  const monaco = useMonaco()
  function clearMonaco() {
    if (monaco) monaco.editor.getModels().forEach((model) => model.dispose())
  }
  const onChange = (event: SelectChangeEvent) => {
    setSolver(event.target.value)
    setObjFile(null)
  }
  function newCase() {
    setSendingCaseStatus('waiting')
    axios
      .get('/api/newcase', { params: { solver: solver, objFile: objFile } })
      .then((response) => {
        dispatch({
          solverName: solver,
          objFileName: objFile,
          caseFiles: response.data || {
            root: {
              index: 'root',
              data: 'Root item',
              hasChildren: true,
              children: [],
              text: ''
            }
          }
        })
        localStorage.setItem('solverName', solver)
        localStorage.setItem('objFileName', objFile)
        localStorage.setItem('caseFiles', JSON.stringify(response.data))
        clearMonaco()
        router.push('/edit')
      })
      .catch((err) => {
        console.error(err)
        setSendingCaseStatus('error')
      })
  }
  function onUploadStart() {
    setIsUploading(true)
  }
  function onUploadFinish() {
    setIsUploading(false)
  }

  function handleClickNext() {
    newCase()
  }

  function handleUploadResponse(response) {
    setObjFile(response.data.filename)
  }

  return (
    <Styled.CenteredText>
      <h2>Select a solver</h2>
      <Styled.Grid gap="1.5em">
        <SolverSelect value={solver} onChange={onChange} />
        <Upload
          title="Upload an obj file (optional)"
          open={solver && solver !== 'hamFoam'}
          accept=".obj"
          handleResponse={handleUploadResponse}
          onUploadFinish={onUploadFinish}
          onUploadStart={onUploadStart}
          endpoint="/api/uploadobj"
        />
        <LoadingButton
          loading={sendingCaseStatus === 'waiting'}
          variant="contained"
          data-testid={'solver-next'}
          disabled={isUploading}
          onClick={handleClickNext}
        >
          Next
        </LoadingButton>
      </Styled.Grid>
      <CustomAlert
        open={sendingCaseStatus === 'error'}
        onClose={() => setSendingCaseStatus('notStarted')}
        severity="error"
      >
        Failed to connect to the server. Please try again.
      </CustomAlert>
    </Styled.CenteredText>
  )
}
