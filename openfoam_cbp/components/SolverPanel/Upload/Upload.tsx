import axios from 'axios'
import React, { useState, useRef, useEffect } from 'react'
import FileUploadIcon from '@mui/icons-material/FileUpload'
import Button from '@mui/material/Button'
import { Progress } from 'components/SolverPanel/Progress'
import * as Styled from 'components/@shared/styles'

export const Upload = ({
  accept,
  onUploadFinish,
  onUploadStart,
  handleResponse,
  title,
  endpoint,
  open
}) => {
  const fileInputRef = useRef<HTMLInputElement | null>(null)
  const [file, setFile] = useState(null)
  const [progress, setProgress] = useState(null)
  function onFileChange(event) {
    setFile(event.target.files[0])
  }
  useEffect(() => {
    if (progress === 100) {
      onUploadFinish()
    }
  }, [progress, onUploadFinish])
  useEffect(() => {
    setFile(new File([], ''))
  }, [])

  function upload() {
    onUploadStart()
    const formData = new FormData()
    formData.append('file', file)
    axios
      .post(endpoint, formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
        onUploadProgress: (data) => {
          setProgress(Math.round((100 * data.loaded) / data.total))
        }
      })
      .then((response) => handleResponse(response))
      .catch((error) => handleResponse(error.response))
  }

  if (!open) {
    return null
  }
  if (progress) {
    return (
      <>
        <Progress progress={progress} />
      </>
    )
  }
  return (
    <div>
      <Styled.BlockSpan>{title}</Styled.BlockSpan>
      <Styled.Grid gap="1em">
        <div>
          <Button
            variant="outlined"
            startIcon={<FileUploadIcon />}
            onClick={() => fileInputRef.current.click()}
          >
            Choose File
          </Button>
          <input
            data-testid="solver-input"
            ref={fileInputRef}
            type="file"
            accept={accept}
            hidden
            onChange={onFileChange}
          />
          {file ? <Styled.BlockSpan>{file.name}</Styled.BlockSpan> : null}
        </div>
        {file && file.name && !progress && (
          <Button
            variant="contained"
            color="success"
            data-testid="solver-upload"
            onClick={upload}
          >
            Upload
          </Button>
        )}
      </Styled.Grid>
    </div>
  )
}
