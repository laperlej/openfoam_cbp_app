import React from 'react'
import * as Styled from './styles'
import { ImportExportDiv } from 'components/@shared/styles'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { Export } from 'components/Export'
import { Import } from 'components/Import'
import { useCaseContext } from 'components/@shared/hooks/useCaseContext'

export const TopNav = () => {
  const { state } = useCaseContext()
  const location = useRouter()
  const pageInfo = [
    { href: '/solver', text: 'Solver' },
    { href: '/edit', text: 'Edit' },
    { href: '/run', text: 'Run' },
    { href: '/log', text: 'Log' },
    { href: '/postprocess', text: 'Postprocess' },
    { href: '/help', text: 'Help' }
  ]
  function pages() {
    return pageInfo.map((info) => {
      return (
        <Link key={info.href} href={info.href}>
          <Styled.NavLink active={location.pathname === info.href}>
            {info.text}
          </Styled.NavLink>
        </Link>
      )
    })
  }
  return (
    <Styled.Nav>
      {pages()}
      {location.pathname == '/edit' ? (
        <ImportExportDiv>
          <Import key={state.caseFiles} />
          <Export key={state.caseFiles} />
        </ImportExportDiv>
      ) : null}
    </Styled.Nav>
  )
}
