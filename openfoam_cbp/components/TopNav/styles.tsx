import styled from '@emotion/styled'

/* Add a black background color to the top navigation */
export const Nav = styled.div`
  background-color: #333 !important;
  line-height: 1.4285em !important;
  font-size: 14px !important;
  font-family: 'Lato', 'Helvetica Neue', Arial, Helvetica, Sans-serif !important;
  overflow: hidden !important;
  min-width: 570px !important;
  position: relative !important;
  display: flex;
`

export const NavLink = styled.a<{ active: boolean }>`
  justify-self: flex-start;
  #float: left !important;
  color: #f2f2f2 !important;
  text-align: center !important;
  padding: 14px 16px !important;
  text-decoration: none !important;
  font-size: 17px !important;
  &:hover {
    cursor: pointer;
    background-color: #ddd !important;
    color: black !important;
  }

  ${({ active }) =>
    active &&
    `
background-color: #1b6b48 !important;
color: white !important;
  `}
`
