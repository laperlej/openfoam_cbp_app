import React, { useReducer, useMemo } from 'react'
import { TopNav } from 'components/TopNav'
import { CaseContext } from 'components/@shared/contexts/CaseContext'
import CssBaseline from '@mui/material/CssBaseline'
import 'styles/global.css'
import { loader } from '@monaco-editor/react'
import Head from 'next/head'
import { emptyState } from 'components/@shared/utils/emptyState'
import { ThemeProvider, createTheme } from '@mui/material/styles'

loader.config({
  paths: { vs: '/monaco-editor/min/vs' }
})

const reducer = (state, action) => {
  return { ...(state || {}), ...(action || {}) }
}

function initState() {
  if (localStorage) {
    return {
      solverName: localStorage?.getItem('solverName') || emptyState.solverName,
      objFileName:
        localStorage?.getItem('objFileName') || emptyState.solverName,
      caseFiles:
        JSON.parse(localStorage?.getItem('caseFiles')) || emptyState.solverName
    }
  }
  return emptyState
}

export default function App({ Component, pageProps }) {
  const [state, dispatch] = useReducer(reducer, emptyState)
  const theme = createTheme({
    palette: {
      primary: {
        main: '#1b6b48'
      }
    }
  })
  const contextValue = useMemo(() => {
    return { state, dispatch }
  }, [state, dispatch])
  React.useEffect(() => {
    dispatch(initState())
  }, [])
  return (
    <React.Fragment>
      <Head>
        <title>OpenFOAM App</title>
      </Head>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <CaseContext.Provider value={contextValue}>
          <TopNav />
          <Component {...pageProps} />
        </CaseContext.Provider>
      </ThemeProvider>
    </React.Fragment>
  )
}
