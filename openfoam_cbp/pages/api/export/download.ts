import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import path from 'path'
import fs from 'fs'

export default withSessionRoute(withLogger(handler))

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //GET
  if (req.method !== 'GET') {
    res.status(405)
    res.end()
    return
  }

  const session = getDataStore().getSession(req.session)

  const basename = path.basename(session.caseDir)
  const fullpath = path.join(session.caseDir, `export-${basename}.tar.gz`)
  let stat
  try {
    stat = fs.statSync(fullpath)
  } catch (e) {
    res.status(401)
    res.end()
    return
  }

  res.writeHead(200, {
    'Content-Type': 'application/x-compressed',
    'Content-Disposition': `attachment; filename="export-${basename}.tar.gz"`,
    'Content-Length': stat.size
  })

  const readStream = fs.createReadStream(fullpath)
  await new Promise(function (resolve) {
    readStream.pipe(res)
    readStream.on('end', resolve)
  })
}
