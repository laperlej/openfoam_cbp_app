import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import { writeCase } from 'server/case/caseWriter'
import { compressCase } from 'server/case/compressCase'
import path from 'path'

export default withSessionRoute(withLogger(handler))

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //POST
  if (req.method !== 'POST') {
    res.status(405)
    res.end()
    return
  }
  const session = getDataStore().getSession(req.session)
  if (!req.body.caseFiles) {
    res.status(422)
    res.end()
    return
  }
  const caseFiles: Record<string, unknown> = req.body.caseFiles
  const solverName: string = req.body.solver
  const baseName = path.basename(session.caseDir)
  const outputPath = path.join(session.caseDir, `export-${baseName}.tar.gz`)
  const rootDir = path.join(session.caseDir, 'export')
  writeCase(session, caseFiles, path.join('export', solverName))
  compressCase(rootDir, outputPath, solverName)

  await req.session.save()
  res.status(204)
  res.end()
}
