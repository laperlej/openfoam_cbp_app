import { collectLogs } from 'server/logs/logcollector'
import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import { autoSSE } from 'server/sse'

export default withSessionRoute(withLogger(handler))

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //GET
  if (req.method !== 'GET') {
    res.status(405)
    res.end()
    return
  }
  const session = getDataStore().getSession(req.session)
  let id = 1
  if (!session.solver) {
    res.status(401)
    res.end()
    return
  }

  function sendData() {
    const logs = collectLogs(session.caseDir, session.solver)
    res.write(`id: ${id++}\nevent: log\ndata: ${JSON.stringify(logs)}\n\n`)
  }

  autoSSE(req, res, sendData)
}

export const config = {
  api: {
    bodyParser: false
  }
}
