import type { NextApiRequest, NextApiResponse } from 'next'
import path from 'path'
import fs from 'fs'
import os from 'os'
import { withLogger } from 'server/middlewares/withLogger'
import busboy from 'busboy'
import { getDataStore } from 'server/session/dataStore'
import { withSessionRoute } from 'server/middlewares/withSession'
import { ExportFile } from 'server/case/CaseReader'

export default withSessionRoute(withLogger(handler))

function handler(req: NextApiRequest, res: NextApiResponse) {
  //POST
  if (req.method !== 'POST') {
    res.status(405)
    res.end()
    return
  }
  return new Promise<void>((resolve) => {
    const session = getDataStore().getSession(req.session)
    const bb = busboy({
      headers: req.headers,
      highWaterMark: 2 * 1024 * 1024
    })
    bb.on('file', (_name, file, info) => {
      const fstream = fs.createWriteStream(
        path.join(os.tmpdir(), info.filename)
      )
      file.pipe(fstream)
      fstream.on('close', () => {
        req.session.save().then(async () => {
          const caseReader = new ExportFile(
            session,
            path.join(os.tmpdir(), info.filename)
          )
          const [caseSolver, importedCaseFiles, objFileName] =
            await caseReader.parse()
          if (Object.keys(importedCaseFiles).length === 0) {
            res.status(400)
            res.end()
            resolve()
            return
          }
          session.newCase(caseSolver, objFileName)
          await req.session.save()
          res.status(200).send({
            solver: caseSolver,
            caseFiles: importedCaseFiles,
            objFileName: objFileName
          })
          res.end()
          resolve()
        })
      })
    })
    req.pipe(bb)
  })
}

export const config = {
  api: {
    bodyParser: false
  }
}
