import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import { getCaseFiles } from 'server/case/caseFiles'
import { TextEncoder, TextDecoder } from 'util'
global.TextEncoder = TextEncoder
global.TextDecoder = TextDecoder

export default withSessionRoute(withLogger(handler))

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //GET
  if (req.method !== 'GET') {
    res.status(405)
    res.end()
    return
  }
  if (!req.query.solver) {
    res.status(401)
    res.end()
    return
  }

  const session = getDataStore().getSession(req.session)
  const solver: string = req.query.solver as string
  const objFile = req.query.objFile
  session.newCase(solver, objFile)
  const caseFiles = getCaseFiles(solver, Boolean(objFile))

  await req.session.save()
  res.status(200).send(caseFiles)
  res.end()
}
