import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import { spawnCaseRunner } from 'server/case/caseRunner'
import { writeRunCase } from 'server/case/caseWriter'

export default withSessionRoute(withLogger(handler))

function runCase(session) {
  session.runStatusListener.updateStatus('running')
  const child = spawnCaseRunner(session.caseDir)
  child.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`)
  })
  child.on('close', (code) => {
    session.runStatusListener.updateStatus(`${code}`)
  })
  session.childProcesses.run = child
}

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //POST
  if (req.method !== 'POST') {
    res.status(405)
    res.end()
    return
  }
  const session = getDataStore().getSession(req.session)
  if (!req.body.caseFiles || !session.caseDir || !session.solver) {
    res.status(401)
    res.end()
    return
  }
  const caseFiles: Record<string, unknown> = req.body.caseFiles
  const multiProcessing = Boolean(req.body.multiProcessing)
  const latestTime = Boolean(req.body.latestTime)

  session.killJobs()
  writeRunCase(session, caseFiles, multiProcessing, latestTime)
  runCase(session)

  await req.session.save()
  res.status(204)
  res.end()
}
