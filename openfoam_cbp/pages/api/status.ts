import type { NextApiRequest, NextApiResponse } from 'next'
import { withSessionRoute } from 'server/middlewares/withSession'
import { getDataStore } from 'server/session/dataStore'
import { withLogger } from 'server/middlewares/withLogger'
import { autoSSE } from 'server/sse'

export default withSessionRoute(withLogger(handler))

async function handler(req: NextApiRequest, res: NextApiResponse) {
  //GET
  if (req.method !== 'GET') {
    res.status(405)
    res.end()
    return
  }

  const session = getDataStore().getSession(req.session)
  let id = 1
  function sendData() {
    res.write(`id: ${id++}\n${session.runStatusListener.updateMessage()}`)
    res.write(`id: ${id++}\n${session.postStatusListener.updateMessage()}`)
  }

  autoSSE(req, res, sendData)
}

export const config = {
  api: {
    bodyParser: false
  }
}
