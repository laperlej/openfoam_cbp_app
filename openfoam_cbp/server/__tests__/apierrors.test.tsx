import { mockRequestResponse } from '../../components/@shared/testUtils/mocks/mockResquest'
import { RequestMethod } from 'node-mocks-http'
import downloadHandler from 'pages/api/download'
import fetchlogsHandler from 'pages/api/fetchlogs'
import loginHandler from 'pages/api/login'
import logoutHandler from 'pages/api/logout'
import newcaseHandler from 'pages/api/newcase'
import postprocessHandler from 'pages/api/postprocess'
import runcaseHandler from 'pages/api/runcase'
import statusHandler from 'pages/api/status'
import uploadobjHandler from 'pages/api/uploadobj'
import exportDownloadHandler from 'pages/api/export/download'
import exportPrepareHandler from 'pages/api/export/prepare'
import type { NextApiHandler } from 'next'

const apisNo401 = [
  ['newcase', newcaseHandler, 'GET'],
  ['uploadobj', uploadobjHandler, 'POST'],
  ['logout', logoutHandler, 'POST'],
  ['login', loginHandler, 'POST'],
  ['status', statusHandler, 'GET'],
  ['fetchlogs', fetchlogsHandler, 'GET']
]

const apis401 = [
  ['download', downloadHandler, 'GET'],
  ['postprocess', postprocessHandler, 'POST'],
  ['runcase', runcaseHandler, 'POST'],
  ['export/download', exportDownloadHandler, 'GET']
]

const apis422 = [['export/prepare', exportPrepareHandler, 'POST']]
describe('test basic errors', () => {
  test.each([].concat(apis401, apisNo401))(
    '405 %s',
    async (_, handler: NextApiHandler, method: RequestMethod) => {
      const query = {}
      const { req, res } = mockRequestResponse(
        method === 'GET' ? 'POST' : 'GET', //wrong method
        query
      )
      await handler(req, res)
      expect(res.statusCode).toBe(405)
    }
  )

  test.each(apis401)(
    '401 %s',
    async (_, handler: NextApiHandler, method: RequestMethod) => {
      const query = {}
      const { req, res } = mockRequestResponse(method, query)
      await handler(req, res)
      expect(res.statusCode).toBe(401)
    }
  )
  test.each(apis422)(
    '401 %s',
    async (_, handler: NextApiHandler, method: RequestMethod) => {
      const query = {}
      const { req, res } = mockRequestResponse(method, query)
      await handler(req, res)
      expect(res.statusCode).toBe(422)
    }
  )
})
