import { mockRequestResponse } from '../../components/@shared/testUtils/mocks/mockResquest'
import prepareHandler from 'pages/api/export/prepare'
import child_process, { SpawnSyncReturns } from 'child_process'
import fs from 'fs'
import { getCaseFiles } from 'server/case/caseFiles'
import { TextEncoder, TextDecoder } from 'util'
global.TextEncoder = TextEncoder
global.TextDecoder = TextDecoder

jest.mock('child_process')
jest.mock('fs')
const mockedChildProcess = child_process as jest.Mocked<typeof child_process>
const mockedFs = fs as jest.Mocked<typeof fs>

describe('/api/export/prepare', () => {
  test('Can prepare a case successfully', async () => {
    const solverName = 'hamFoam'
    const query = {
      solver: solverName,
      caseFiles: getCaseFiles(solverName)
    }
    mockedChildProcess.spawnSync.mockImplementationOnce((_a, _b, _c) => {
      return {} as SpawnSyncReturns<string | Buffer>
    })
    mockedFs.mkdtempSync.mockImplementation((_) => '/tmp')
    const { req, res } = mockRequestResponse('POST', query)
    await prepareHandler(req, res)
    expect(res.statusCode).toBe(204)
  })
})
