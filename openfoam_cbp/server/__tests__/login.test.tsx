import { mockRequestResponse } from '../../components/@shared/testUtils/mocks/mockResquest'
import loginHandler from 'pages/api/login'
import { TextEncoder, TextDecoder } from 'util'
global.TextEncoder = TextEncoder
global.TextDecoder = TextDecoder

describe('/api/login', () => {
  test('returns a new case', async () => {
    const query = {}
    const { req, res } = mockRequestResponse('POST', query)
    await loginHandler(req, res)

    expect(res.statusCode).toBe(204)
  })
})
