import { mockRequestResponse } from '../../components/@shared/testUtils/mocks/mockResquest'
import logoutHandler from 'pages/api/logout'
import { TextEncoder, TextDecoder } from 'util'
global.TextEncoder = TextEncoder
global.TextDecoder = TextDecoder

describe('/api/logout', () => {
  test('returns a new case', async () => {
    const query = {}
    const { req, res } = mockRequestResponse('POST', query)
    await logoutHandler(req, res)

    expect(res.statusCode).toBe(204)
  })
})
