import { mockRequestResponse } from '../../components/@shared/testUtils/mocks/mockResquest'
import newCaseHandler from 'pages/api/newcase'

describe('/api/newcase', () => {
  test('returns a new case', async () => {
    const query = {
      solver: 'hamFoam',
      objFile: ''
    }
    const { req, res } = mockRequestResponse('GET', query)
    await newCaseHandler(req, res)

    expect(res.statusCode).toBe(200)
  })
})
