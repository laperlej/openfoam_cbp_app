import { Session } from '../session/session'
import { once } from 'events'
import { getBlankCaseFiles } from './caseFiles'
import os from 'os'
import path from 'path'
import fs from 'fs'
import gunzip from 'gunzip-maybe'
import tar from 'tar-stream'

//Takes in a .tar.gz file generated by the export api endpoint and returns a casefiles json that can be used by the client
export class ExportFile {
  _filePath: string
  _extract
  _session: Session

  constructor(session, exportFilePath) {
    this._filePath = exportFilePath
    this._extract = tar.extract()
    this._session = session
  }

  async parse() {
    if (!fs.existsSync(this._filePath)) {
      console.error(`File ${this._filePath} doesn't exist`)
    }

    let solverName = ''
    let objFileName = ''
    let caseFiles = undefined
    const fstream = fs.createReadStream(this._filePath)
    let objFilePath = ''

    //read tar.gz filestream
    //https://www.npmjs.com/package/tar-stream
    this._extract.on('entry', function (header, stream, next) {
      // header is the tar header
      // stream is the content body (might be an empty stream)
      // call next when you are done with this entry
      if (header.type === 'file') {
        if (caseFiles === undefined) {
          solverName = header.name.split('/')[0]
          caseFiles = getBlankCaseFiles(solverName)
        }
        const fileName = header.name.replace(`${solverName}/`, '')
        stream.on('data', function (response) {
          if (caseFiles?.[fileName]?.['text'] !== undefined) {
            if (caseFiles[fileName]['data'] === 'buildings.obj') {
              objFilePath = fileName
            }
            caseFiles[fileName]['text'] += response.toString('utf-8')
          }
        })
      }

      stream.on('end', function () {
        next() // ready for next entry
      })

      stream.resume() // just auto drain the stream
    })

    fstream.pipe(gunzip()).pipe(this._extract)
    await once(this._extract, 'finish')
    // deal with obj file if there is one
    // the buildings.obj file will be blank and a file called exportedObjFile.obj will be put in /tmp
    if (objFilePath !== '') {
      objFileName = 'exportedObjFile.obj'
      const objPath = path.join(os.tmpdir(), objFileName)
      fs.writeFileSync(objPath, caseFiles[objFilePath]['text'])
      this._session.newCase(solverName, objFileName)
      caseFiles[objFilePath]['text'] = ''
    }

    return [solverName, caseFiles, objFileName]
  }
}

//export function readCase(session, filepath) {
