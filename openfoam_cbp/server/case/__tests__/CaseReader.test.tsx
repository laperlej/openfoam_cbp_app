import { ExportFile } from '../CaseReader'
import { getCaseFiles } from '../caseFiles'
import { Session } from '../../session/session'

const exportSamplePath = './server/case/__tests__/export.tar.gz'
const solverName = 'urbanMicroclimateFoam'

describe('CaseReader', () => {
  test('read sample export file', async () => {
    const caseFiles = getCaseFiles(solverName)
    const session = new Session('fakeid')

    const caseReader = new ExportFile(session, exportSamplePath)
    const [caseSolver, importedCaseFiles] = await caseReader.parse()
    expect(importedCaseFiles).toStrictEqual(caseFiles)
    expect(caseSolver).toStrictEqual(solverName)
  })
})
