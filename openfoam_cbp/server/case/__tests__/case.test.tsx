import { writeCase } from '../caseWriter'
import { getCaseFiles } from '../caseFiles'

jest.mock('fs')

const solvers = ['hamFoam', 'windDrivenRainFoam', 'urbanMicroclimateFoam']

describe('EditPanel', () => {
  test.each(solvers)('writes a %s case', (solverName) => {
    const caseFiles = getCaseFiles(solverName)
    const session = {
      solver: solverName,
      caseDir: '',
      objFile: ''
    }
    writeCase(session, caseFiles)
  })
  test.each(solvers.slice(1))('writes a %s case with obj', (solverName) => {
    const caseFiles = getCaseFiles(solverName, true)
    const session = {
      solver: solverName,
      caseDir: '',
      objFile: 'buildings.obj'
    }
    writeCase(session, caseFiles)
  })
})
