// Generates the appropriate Allrun shell file that will be called to run the case
// fileIndex is used by multi-solver cases with multiple Allrun files
export function getAllRun(
  caseName: string,
  fileIndex: string,
  isMulti,
  objFile,
  latestTime
) {
  const latestTimeFlag = latestTime ? ' -latestTime' : ''
  const withFlag = `runApplication reconstructPar${latestTimeFlag}`
  const reconstructPar = `${isMulti ? withFlag : ''}`
  if (caseName === 'hamFoam') {
    return getHamAllRun(isMulti, reconstructPar)
  }
  if (caseName === 'urbanMicroclimateFoam') {
    return getUrbanAllRun(isMulti, objFile, reconstructPar)
  }
  if (caseName === 'windDrivenRainFoam') {
    if (fileIndex === 'Allrun') {
      return getMainWindAllRun()
    }
    if (fileIndex === 'simpleFoam/Allrun') {
      return getSimpleAllRun(isMulti, objFile, reconstructPar)
    }
    if (fileIndex === 'windDrivenRainFoam/Allrun') {
      return getWindAllRun(isMulti, reconstructPar)
    }
    throw new Error('Unexpected fileIndex' + fileIndex)
  }
  throw new Error('Unexpected caseName: ' + caseName)
}

function getReconstructPar(isMulti, latestTime) {
  const latestTimeFlag = latestTime ? ' -latestTime' : ''
  const withFlag = `runApplication reconstructPar${latestTimeFlag}`
  return `${isMulti ? withFlag : ''}`
}

function getHamAllRun(isMulti, latestTime) {
  return `#!/bin/sh
cd \${0%/*} || exit 1
set -e
. $WM_PROJECT_DIR/bin/tools/RunFunctions
runApplication blockMesh
runApplication setSet -batch system/setset.batch
${isMulti ? 'runApplication decomposePar' : ''}
${isMulti ? 'runParallel' : 'runApplication'} \`getApplication\`
${getReconstructPar(isMulti, latestTime)}`
}

function getSimpleAllRun(isMulti, objFile, reconstructPar) {
  return `#!/bin/sh
cd \${0%/*} || exit 1
set -e
. $WM_PROJECT_DIR/bin/tools/RunFunctions
${objFile ? 'runApplication surfaceFeatures' : ''}
runApplication blockMesh
${objFile ? 'runApplication snappyHexMesh -overwrite' : ''}
${isMulti ? 'runApplication decomposePar' : ''}
${isMulti ? 'runParallel' : 'runApplication'} \`getApplication\`
${reconstructPar}`
}

function getWindAllRun(isMulti, reconstructPar) {
  return `#!/bin/sh
cd \${0%/*} || exit 1
set -e
. $WM_PROJECT_DIR/bin/tools/RunFunctions
cp -r ../simpleFoam/constant/polyMesh/ constant/
for time in $(foamListTimes -case ../simpleFoam)
do
[ "$time" = "0" -o "$time" = constant ] || {
    timeDir=$time
  '    echo "Copying files U, k, epsilon, nut from directory $timeDir"
    cp -r ../simpleFoam/\${timeDir}/U.gz 0/U.gz
    cp -r ../simpleFoam/\${timeDir}/k.gz 0/k.gz
    cp -r ../simpleFoam/\${timeDir}/epsilon.gz 0/epsilon.gz
    cp -r ../simpleFoam/\${timeDir}/nut.gz 0/nut.gz
}
done
echo "Running changeDictionary app..."
changeDictionary > log.changeDictionary
${isMulti ? 'runApplication decomposePar' : ''}
${isMulti ? 'runParallel' : 'runApplication'} \`getApplication\`
${reconstructPar}
`
}

function getMainWindAllRun() {
  return `#!/bin/sh
cd \${0%/*} || exit 1
set -e
simpleFoam/Allrun
windDrivenRainFoam/Allrun`
}

function getUrbanAllRun(isMulti, objFile, reconstructPar) {
  return `#!/bin/sh
cd \${0%/*} || exit 1
set -e
. $WM_PROJECT_DIR/bin/tools/RunFunctions
${isMulti || objFile ? 'ln -s air/polyMesh constant/polyMesh' : ''}
${objFile ? 'ln -s air/fvSchemes system/fvSchemes' : ''}
${objFile ? 'ln -s air/fvSolution system/fvSolution' : ''}
${objFile ? 'runApplication surfaceFeatures' : ''}
runApplication blockMesh -region air
runApplication createPatch -region air -overwrite
${objFile ? 'runApplication snappyHexMesh -overwrite' : ''}
${isMulti ? 'runApplication decomposePar -region air' : ''}
${isMulti ? 'runParallel' : 'runApplication'} \`getApplication\`
${reconstructPar}`
}
