import hamTemplate from './templates/ham5.json'
import urbanTemplate from './templates/urban_cfd.json'
import urbanObjTemplate from './templates/urban_cfd_obj.json'
import windTemplate from './templates/wind.json'
import windObjTemplate from './templates/wind_obj.json'

const templates = {
  '': hamTemplate,
  hamFoam: hamTemplate,
  urbanMicroclimateFoam: urbanTemplate,
  urbanMicroclimateFoam_obj: urbanObjTemplate,
  windDrivenRainFoam: windTemplate,
  windDrivenRainFoam_obj: windObjTemplate
}

//Returns the template for the case as an object that can be used by the client
export const getCaseFiles = (solver: string, withObj = false) => {
  let newData = {}
  try {
    if (withObj) {
      newData = JSON.parse(JSON.stringify(templates[solver + '_obj']))
    } else {
      newData = JSON.parse(JSON.stringify(templates[solver]))
    }
  } catch (err) {
    console.log(err)
  }
  return newData
}

export const getBlankCaseFiles = (solver: string, withObj = false) => {
  const caseFiles = getCaseFiles(solver, withObj)
  Object.keys(caseFiles).map((filename) => (caseFiles[filename]['text'] = ''))
  return caseFiles
}
