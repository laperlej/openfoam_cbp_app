import { getAllRun } from './allrun'
import path from 'path'
import fs from 'fs'

function addFile(files, filePath, text) {
  const fileInfo = getFileInfo(filePath, text)
  files[filePath] = fileInfo
  const dir = path.dirname(filePath).replace('.', '') || 'root'
  files[dir].children.push(filePath)
}

function getFileInfo(filePath, text) {
  return {
    index: filePath,
    data: path.basename(filePath),
    text: text,
    hasChildren: false,
    children: []
  }
}

function prepAllrun(files, caseName, multiProcessing, latestTime, objFile) {
  addFile(
    files,
    'Allrun',
    getAllRun(caseName, 'Allrun', multiProcessing, objFile, latestTime)
  )
  if (caseName === 'windDrivenRainFoam') {
    addFile(
      files,
      'simpleFoam/Allrun',
      getAllRun(
        caseName,
        'simpleFoam/Allrun',
        multiProcessing,
        objFile,
        latestTime
      )
    )
    addFile(
      files,
      'windDrivenRainFoam/Allrun',
      getAllRun(
        caseName,
        'windDrivenRainFoam/Allrun',
        multiProcessing,
        objFile,
        latestTime
      )
    )
  }
}

function makeDir(dirPath) {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true })
  }
}

function createFile(session, fileInfo, filePath) {
  if (session.objFile && fileInfo.data === 'buildings.obj') {
    const objFilePath = path.join(session.objFile)
    if (fs.existsSync(objFilePath)) {
      fs.copyFileSync(objFilePath, filePath)
    }
  } else {
    fs.writeFileSync(filePath, fileInfo.text)
  }
  if (['Allrun', 'Allclean', 'Allprepare'].includes(fileInfo.data)) {
    fs.chmodSync(filePath, 0o700)
  }
}

export function writeRunCase(
  session,
  caseFiles,
  multiProcessing = false,
  latestTime = false
) {
  prepAllrun(
    caseFiles,
    session.solver,
    multiProcessing,
    latestTime,
    session.objFile
  )
  writeCase(session, caseFiles)
}

export function writeCase(session, caseFiles, subFolder = '') {
  const toDo = ['root']
  const rootPath = path.join(session.caseDir, subFolder)
  makeDir(session.caseDir)
  makeDir(rootPath)
  while (toDo.length) {
    const current_dir = caseFiles[toDo.pop()]
    for (const child of current_dir['children']) {
      const childPath = path.join(session.caseDir, subFolder, child)
      if (caseFiles[child].hasChildren) {
        makeDir(childPath)
        toDo.push(child)
      } else {
        createFile(session, caseFiles[child], childPath)
      }
    }
  }
}
