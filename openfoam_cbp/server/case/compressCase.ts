import { spawnSync } from 'child_process'

export const compressCase = (rootDir, outputPath, solver) => {
  const cmd = ['tar -czvf', outputPath, solver, '>>../log.export 2>&1'].join(
    ' '
  )
  return spawnSync(cmd, [], {
    stdio: 'pipe',
    shell: '/bin/bash',
    cwd: rootDir
  })
}
