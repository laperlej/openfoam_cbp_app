import morgan from 'morgan'
import type { NextApiRequest, NextApiResponse } from 'next'

const logger = morgan('common')
function withLoggerApiRoute(handler) {
  return async function (req: NextApiRequest, res: NextApiResponse) {
    logger(req, res, () => {
      /* no callback */
    })
    return handler(req, res)
  }
}

export function withLogger(handler) {
  return withLoggerApiRoute(handler)
}
