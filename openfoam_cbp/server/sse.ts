export const autoSSE = (req, res, sendData, interval = 1000) => {
  res.writeHead(200, {
    Connection: 'keep-alive',
    'Content-Type': 'text/event-stream',
    'Content-Encoding': 'none',
    'Cache-Control': 'no-cache',
    'X-Accel-Buffering': 'no'
  })

  let intervalID = null

  const end = () => {
    if (intervalID) {
      clearTimeout(intervalID)
    }
  }
  req.on('aborted', end)
  req.on('close', end)

  intervalID = setInterval(sendData, interval)
}
