import os
import json
import sys
import argparse


def add_dirname(os_walk_result: list) -> list:
    dir_path = os_walk_result[0]
    def add_path(child_name): return os.path.join(dir_path, child_name)
    os_walk_result[1] = list(map(add_path, os_walk_result[1]))
    os_walk_result[2] = list(map(add_path, os_walk_result[2]))


class Case():
    def __init__(self, os_walk_results: list, root_dir: str) -> None:
        """Turns a os.walk result into a json that can be used by react-tree-view."""
        self.file_dict = {}
        self.root_dir = root_dir
        for os_walk_result in os_walk_results:
            add_dirname(os_walk_result)
            self._add_dir(os_walk_result)
            self._add_case_files(os_walk_result[2])

    def make_child_path(self, path):
        return self._remove_root_from_path(path)

    def _add_dir(self, os_walk_result: list) -> None:
        dir_path = os_walk_result[0]
        if dir_path == self.root_dir:
            dir_path = 'root'
        dir_name = os.path.basename(dir_path)
        dir_index = self._remove_root_from_path(dir_path)
        dir_children = os_walk_result[1] + os_walk_result[2]
        self.file_dict[dir_index] = {"index": dir_index,
                                     "data": dir_name,
                                     "children": list(map(self.make_child_path, dir_children)),
                                     "hasChildren": True,
                                     "text": ''}

    def _add_case_files(self, file_paths: list) -> None:
        for file_path in file_paths:
            self._add_case_file(file_path)

    def _add_case_file(self, file_path: str) -> None:
        file_name = os.path.basename(file_path)
        file_text = open(file_path).read()
        file_index = self._remove_root_from_path(file_path)
        self.file_dict[file_index] = {"index": file_index,
                                      "data": file_name,
                                      "children": [],
                                      "hasChildren": False,
                                      "text": file_text}

    def _remove_root_from_path(self, path: str) -> str:
        return path.replace(self.root_dir, '', 1)

    def to_json(self) -> str:
        return json.dumps(self.file_dict)

    def to_jsx(self) -> str:
        return "const data = " + self.to_json() + ";\n\nexport default data;"


def add_trailing_slash(root_dir):
    if root_dir[-1] != "/":
        return root_dir + "/"
    return root_dir


def os_walk_wrapper(root_dir):
    os_walk_results = os.walk(root_dir)
    os_walk_list = list(map(list, os_walk_results))
    return os_walk_list


def fetch_directory_structure(root_dir):
    os_walk_results = os_walk_wrapper(root_dir)
    return os_walk_results


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("root_dir", type=str,
                           help="Root directory to start from")
    args = argparser.parse_args(sys.argv[1:])
    root_dir = add_trailing_slash(args.root_dir)
    dir_structure = fetch_directory_structure(root_dir)
    print(Case(dir_structure, root_dir).to_json())


if __name__ == "__main__":
    main()
