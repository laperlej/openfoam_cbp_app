import os
import json
import sys
import argparse


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def write_file(path, content):
    with open(path, 'w') as f:
        f.write(content)


def write_case(root_dir, file_dict):
    for file_path, file_info in file_dict.items():
        if file_path == "root":
            path = root_dir
        else:
            path = os.path.join(root_dir, file_path)
        if file_info["hasChildren"]:
            mkdir(path)
        else:
            write_file(path, file_info['text'])


def parse_json(json_file):
    json_file = json_file.replace("const data = ", '')
    json_file = json_file.replace(';\n\nexport default data;', '')
    file_dict = json.loads(json_file)
    return file_dict


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("root_dir", type=str,
                           help="Root directory name")
    argparser.add_argument("json_file", type=str,
                           help="json file")
    args = argparser.parse_args(sys.argv[1:])
    root_dir = args.root_dir
    json_file = args.json_file
    file_dict = parse_json(open(json_file).read())
    write_case(root_dir, file_dict)


if __name__ == "__main__":
    main()
