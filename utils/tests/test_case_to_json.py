import unittest
import case_to_json


class TestCaseToJson(unittest.TestCase):
    def test_add_trailling_slash_no_need(self):
        root_dir = "./"
        new_root_dir = case_to_json.add_trailing_slash(root_dir)
        self.assertEqual(new_root_dir, root_dir)

    def test_add_trailling_slash_need(self):
        root_dir = "."
        new_root_dir = case_to_json.add_trailing_slash(root_dir)
        self.assertEqual(new_root_dir, "./")

    def test_case_to_json(self):
        root_dir = "test/"
        test_walk = [['test/', ['hello'], []], ['test/hello', [], []]]
        case = case_to_json.Case(test_walk, root_dir)
        expected_json = '{"root": {"index": "root", "data": "root", "children": ["hello"], "hasChildren": true, "text": ""}, "test/hello": {"index": "hello", "data": "hello", "children": [], "hasChildren": true, "text": ""}}'
        self.assertEqual(
            case.to_json(), expected_json
        )
