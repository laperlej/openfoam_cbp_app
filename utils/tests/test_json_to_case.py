import unittest
import json_to_case


class TestCaseToJson(unittest.TestCase):
    def test_parse_jsx(self):
        test_json = '{"root": {"index": "root", "data": "root", "children": ["hello"], "hasChildren": true, "text": ""}, "test/hello": {"index": "hello", "data": "hello", "children": [], "hasChildren": true, "text": ""}}'
        expected_json = {"root": {"index": "root", "data": "root", "children": [
            "hello"], "hasChildren": True, "text": ""}, "test/hello": {"index": "hello", "data": "hello", "children": [], "hasChildren": True, "text": ""}}
        result = json_to_case.parse_json(test_json)
        self.assertEqual(result, expected_json)
